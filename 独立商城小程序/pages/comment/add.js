// pages/comment/add.js
var canClick = true;
const app = getApp();
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    orderid:0,
    currentLevel:5,
    comment:"",
    
  },
  choseLevel:function(e){
    this.setData({
      currentLevel: e.currentTarget.dataset.level
    });
  },
  commentChange: function (event) {
    //console.log(event.detail.value);
    this.setData({
      "comment": event.detail.value
    });

  },
  submitComment:function(){
    var that = this;
    if (!canClick)
      return false;
    if(canClick){
      if (this.data.comment == "") {
        wx.showToast({
          title: '请输入评价内容',
          icon: 'none'
        })
        canClick = true;
        return false;
      }
      wx.showLoading({
        title: '提交评价中...',
      })
      wx.request({
        url: app.appSetting.host + "/Comment",
        data: {
          OrderId:that.data.orderid,
          Spid :app.appSetting.spid,
          Said : app.appSetting.said,
          UserId :app.globalData.userInfo.Id,
          CommentMsg:that.data.comment,
          CommentLevel: that.data.currentLevel
        },
        method: 'POST',
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: function (res) {
          console.log(res);
          wx.hideLoading();
          if (res.statusCode== 200) {
            canClick = true;
            wx.showToast({
              title: "提交成功",
              icon: 'none'
            })
           
            
           
          } else {
            wx.showToast({
              title: res.data,
              icon: 'none'
            })
            
            canClick = true;
          }
        }
      })
    }  
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.orderid);
    this.setData({
      orderid: options.orderid
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})