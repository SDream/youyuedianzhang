﻿using CqCore.DB;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace DbService
{
    public class SqlService<TDb> : SqlService, ISqlService<TDb>
        where TDb : IBaseDb
    {
        public SqlService(string conStr) : base(conStr) { }
    }

    public class SqlService : SqlHelper, ISqlService
    {
        private readonly string _conn = string.Empty;

        public SqlService(string connStr)
        {
            _conn = connStr;
        }
        public async Task<SqlParameter> GetRtnParameter()
        {
            SqlParameter rtn_err = new SqlParameter("rtn_err", SqlDbType.Int);
            rtn_err.Direction = ParameterDirection.Output;
            return rtn_err;
        }

        public async Task<StringBuilder> CreateSb()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" 1=1 ");
            return sb;
        }

        /// <summary>
        /// 查询单个数据值
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <returns></returns>
        public async Task<object> ExecuteScalar(string sqlStr, params SqlParameter[] commandParameters)
        {
            return await base.ExecuteScalar(_conn, CommandType.Text, sqlStr, commandParameters);
        }

        /// <summary>
        /// 非查询操作，commandType可能是存储过程或SQL语句
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public async Task<int> ExecuteNonQuery(CommandType type, string sqlOrStoredProcedure, params SqlParameter[] commandParameters)
        {
            return await base.ExecuteNonQuery(_conn, type, sqlOrStoredProcedure, commandParameters);
        }
        /// <summary>
        /// 查询结果集
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public async Task<DataSet> ExecuteDataset(CommandType type, string sqlOrStoredProcedure, params SqlParameter[] commandParameters)
        {
            return await base.ExecuteDataset(_conn, type, sqlOrStoredProcedure, commandParameters);
        }
        /// <summary>
        /// 查询结果集
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public async Task<SqlDataReader> ExecuteReader(CommandType cmdType, string sqlOrStoredProcedure, params SqlParameter[] commandParameters)
        {
            return await base.ExecuteReader(_conn, cmdType, sqlOrStoredProcedure, commandParameters);
        }
        /// <summary>
        /// 事务执行,没行SQL对应一组参数化
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        public async Task<bool> ExecTransactionAsUpdate(string[] sql, List<SqlParameter[]> paramers = null)
        {
            return await base.ExecTransactionAsUpdate(sql, _conn, paramers);
        }
        /// <summary>
        /// 事务执行，所有SQL公用一组参数化
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        public async Task<bool> ExecTransactionAsUpdate_Public(string[] sql, SqlParameter[] paramers = null)
        {
            return await base.ExecTransactionAsUpdate_Public(sql, _conn, paramers);
        }
    }
}
