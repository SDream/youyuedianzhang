﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Area
{
    public class AreaViewModel
    {
        public int Id { get; set; }
        public int Area_Code { get; set; }
        public int Parent_Id { get; set; }
        public string Area_Name { get; set; }
    }
}
