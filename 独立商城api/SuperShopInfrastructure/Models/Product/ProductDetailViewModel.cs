﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class ProductDetailViewModel
    {
        public ProductDetailViewModel()
        {
            this.SkuManagerModel = new ProductSkuManagerModel();
        }
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string Title { get; set; }
        public int MenuId { get; set; }
        public int ShipTemplateId { get; set; }
        public string ShipAmount { get; set; }
        public string Unit { get; set; }
        public string Detail { get; set; }
        public List<string> BannerImgs { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
        public int CurrentSkuType { get; set; }
        public string AmountRank
        {
            get
            {
                return MinAmount.ToString() + " - " + MaxAmount.ToString();
            }
        }

        public ProductSkuManagerModel SkuManagerModel { get; set; }
    }

    public class ProductSkuManagerModel
    {
        public ProductSkuManagerModel()
        {
            this.SingleSkuModel = new SingleSkuModel();
            this.MoreSkuModelList = new List<MoreSkuModel>();
            this.Product_Sall_PropetyModelList = new List<Product_Sall_PropetyModel>();
            this.Product_Sall_Propety_ValueModelList = new List<Product_Sall_Propety_ValueModel>();
        }
        public string ProductId { get; set; }
        /// <summary>
        /// 当前商品是单规格还是多规格
        /// </summary>
        public int SkuType { get; set; }
        /// <summary>
        /// 单规格实体模型
        /// </summary>
        public SingleSkuModel SingleSkuModel { get; set; }
        /// <summary>
        /// 多规格实体模型集合
        /// </summary>
        public List<MoreSkuModel> MoreSkuModelList { get; set; }
        /// <summary>
        /// 当前商品多规格下的属性模型集合
        /// </summary>
        public List<Product_Sall_PropetyModel> Product_Sall_PropetyModelList { get; set; }
        /// <summary>
        /// 当前商品多规格下的属性值模型集合
        /// </summary>
        public List<Product_Sall_Propety_ValueModel> Product_Sall_Propety_ValueModelList { get; set; }
    }

    public class SingleSkuModel
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        public int StockNum { get; set; }
        public decimal SallPrice { get; set; }
        public int SkuType { get; set; }
        public string SkuCode { get; set; }

        public float Weight { get; set; }

        public int ShopAdminId { get; set; }
    }

    public class MoreSkuModel
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        /// <summary>
        /// 属性值ID组合如：100-200-1000
        /// </summary>
        public string PropetyCombineId { get; set; }
        /// <summary>
        /// 销售属性值组合红色+36cm+4G
        /// </summary>
        public string ProppetyCombineName { get; set; }

        public int StockNum { get; set; }
        public decimal SallPrice { get; set; }
        public int SkuType { get; set; }
        public string SkuCode { get; set; }
        public float Weight { get; set; }

    }

    public class Product_Sall_PropetyModel
    {
        public Product_Sall_PropetyModel()
        {
            this.Product_Sall_Propety_ValueList = new List<Product_Sall_Propety_ValueModel>();
        }
        public int Id { get; set; }
        public string PropetyName { get; set; }
        public string ProductId { get; set; }
        public List<Product_Sall_Propety_ValueModel> Product_Sall_Propety_ValueList { get; set; }

    }

    public class Product_Sall_Propety_ValueModel
    {
        public int Id { get; set; }
        public int SallPropetyId { get; set; }
        public string PropetyValue { get; set; }
        public string ProductId { get; set; }
    }
}
