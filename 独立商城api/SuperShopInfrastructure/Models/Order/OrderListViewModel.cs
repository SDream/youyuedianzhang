﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Order
{
    public class OrderListViewModel
    {
        public int OrderID { get; set; }
        public string OrderNo { get; set; }
        public int ProductNum { get; set; }
        public decimal RealityAmount { get; set; }
        public int OrderStatus { get; set; }
        /// <summary>
        /// 订单详情中第一个商品得ID
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 订单详情中第一个商品名称，以下类推
        /// </summary>
        public string ProductName { get; set; }
        public string ProppetyCombineName { get; set; }
        public int BuyNum { get; set; }
        public decimal SalePrice { get; set; }
        public string ImagePath { get; set; }
        public string Said { get; set; }
        /// <summary>
        /// 评价ID
        /// </summary>
        public int CommentId { get; set; }
    }
}
