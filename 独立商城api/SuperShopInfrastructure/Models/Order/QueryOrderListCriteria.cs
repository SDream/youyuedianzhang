﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SuperShopInfrastructure.Models.Order
{
    public class QueryOrderListCriteria
    {
        [Required]
        [Decrypt]
        public string Spid { get; set; }
        [Required]
        [Decrypt]
        public string UserId { get; set; }
        public int OffSet { get; set; }
        public int Size { get; set; }
        /// <summary>
        /// 0，待付款，1，（已发货）订单成功，2订单失败（15分钟未支付），3已付款，4申请售后订单，999全部
        /// </summary>
        public int OrderStatus { get; set; }
    }
}
