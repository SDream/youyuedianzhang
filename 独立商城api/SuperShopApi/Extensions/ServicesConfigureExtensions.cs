﻿using Microsoft.Extensions.DependencyInjection;
using SuperShopInfrastructure.Services;
using SuperShopRepository;
using SuperShopService.IServices;
using SuperShopService.Services;

namespace SuperShopApi.Extensions
{
    public static class ServicesConfigureExtensions
    {
        public static IServiceCollection AddBusinessServices(this IServiceCollection services)
        {
            #region RepositoryServices
            services.AddScoped<UserRepository>();
            services.AddScoped<AreaRepository>();
            services.AddScoped<CommentRepository>();
            services.AddScoped<ConsigneeRepository>();
            services.AddScoped<MarketingRepository>();
            services.AddScoped<OrderRepository>();
            services.AddScoped<ProductRepository>();
            services.AddScoped<QuestionRepository>();
            services.AddScoped<SearchKeywordRepository>();
            services.AddScoped<SetRepository>();
            services.AddScoped<ShopAppRepository>();
            services.AddScoped<ShopCartRepository>();

            #endregion

            #region Services
            services.AddSingleton<DrawQRImageService>();
            services.AddScoped<IAreaService, AreaService>();
            services.AddScoped<ICommentService, CommentService>();
            services.AddScoped<IConsigneeService, ConsigneeService>();
            services.AddScoped<IMarketingService, MarketingService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<ISearchKeywordService, SearchKeywordService>();
            services.AddScoped<ISetService, SetService>();
            services.AddScoped<IShopAppService, ShopAppService>();
            services.AddScoped<IShopCartService, ShopCartService>();
            services.AddScoped<IUserService, UserService>();
            #endregion

            return services;
        }
    }
}
