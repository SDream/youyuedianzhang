﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.SearchKeyword;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SearchKeywordController : ControllerBase
    {
        private readonly ISearchKeywordService searchKeywordService;

        public SearchKeywordController(ISearchKeywordService searchKeywordService)
        {
            this.searchKeywordService = searchKeywordService;
        }

        /// <summary>
        /// 获取搜索词
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(List<SearchWordViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSearchKeyword([FromQuery] string said)
        {
            var _said = ToolManager.DecryptParms(said);
            if (_said == null)
                return BadRequest("非法参数");
            var res = await searchKeywordService.GetList(_said.ToInt());
            return Ok(res);
        }
    }
}