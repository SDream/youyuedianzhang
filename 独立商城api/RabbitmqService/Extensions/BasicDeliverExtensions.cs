﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RabbitmqService.Extensions
{
    public static class BasicDeliverExtensions
    {
        public static bool KeepAttempts(this BasicDeliverEventArgs context, int max)
        {
            if (context.BasicProperties.Headers != null && context.BasicProperties.Headers.ContainsKey("x-death"))
            {
                var deathInfo = context.BasicProperties.Headers["x-death"] as IEnumerable<Object>;
                var deathSummary = deathInfo.FirstOrDefault() as IDictionary<string, object>;
                return Convert.ToInt32(deathSummary["count"]) < max;
            }
            else
            {
                return max > 0;
            }
        }
    }
}
