﻿namespace RabbitmqService.Options
{
    public class RabbitmqPublisherOption
    {
        public string HostName { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ExchangeName { get; set; }
        public bool Persistent { get; set; } = true;

        public int StartRetryInterval { get; set; } = 5;
    }
}
