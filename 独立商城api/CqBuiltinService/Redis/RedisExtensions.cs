﻿using CqCore.Logging;
using CqCore.Redis;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RedisService.Options;

namespace CqBuiltinService.Redis
{
    public static class RedisExtensions
    {
        public static IServiceCollection AddRedis(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var option = provider.GetRequiredService<IOptions<RedisOption>>().Value;
            var logger = provider.GetRequiredService<ILogRecorder<RedisService.RedisService>>();
            var redisService = new RedisService.RedisService(option,logger);
            services.AddSingleton<IRedisService>(redisService);
            return services;
        }
    }
}
