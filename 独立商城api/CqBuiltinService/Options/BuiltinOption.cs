﻿using CqBuiltinService.Environments;
using CqCore.Extensions;
using System;
using System.IO;

namespace CqBuiltinService.Options
{
    public class RuntimeOption
    {
        public string CacheRootDirectory { get; set; }
        public string ModuleName { get; set; } = AppDomain.CurrentDomain.GetApplicationName();
        public string ModuleCacheDirectory => Path.Combine(CacheRootDirectory, ModuleName);
        public string NodeName => Variables.NODE_NAME ?? Variables.COMPUTERNAME;
    }

    public class LoggingOption
    {
        public string ConfigPath { get; set; }
        public string DirectoryName { get; set; } = "logs";
    }


    public class DocumentOption
    {
        public string Version { get; set; }
    }

    public class RequestOption
    {
        public bool Debug { get; set; } = false;
    }
}
