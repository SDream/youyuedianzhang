﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.IO;

namespace CqBuiltinService.Extensions
{
    public static class HttpContextExtensions
    {
        public static IDictionary<string, object> ToDataDictionary(this ModelStateDictionary context)
        {
            var parameters = new Dictionary<string, object>();

            foreach (var item in context)
            {
                parameters.Add(item.Key, item.Value.RawValue);
            }

            return parameters;
        }

        public static string ToText(this Stream context)
        {
            try
            {
                context.Seek(0, System.IO.SeekOrigin.Begin);
                return new StreamReader(context).ReadToEnd();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
    }
}
