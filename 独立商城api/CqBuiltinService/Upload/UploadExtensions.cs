﻿using CqCore.UploadService;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using UploadService.Option;

namespace CqBuiltinService.Upload
{
    public static class UploadExtensions
    {
        public static IServiceCollection AddUploadService(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var options = provider.GetRequiredService<IOptions<ImageServiceOption>>().Value;
            var uoloadService = new UploadService.UploadService(options);
            services.AddSingleton<IUploadService>(uoloadService);
            return services;
        }
    }
}
