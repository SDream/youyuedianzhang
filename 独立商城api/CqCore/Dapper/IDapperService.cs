﻿using System.Data.SqlClient;
using System.Threading.Tasks;

namespace CqCore.Dapper
{
    public interface IDapperService
    {
        Task<TResult> Query<TResult>(string sql, params SqlParameter[] commandParameters);
    }
}
