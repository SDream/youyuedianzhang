﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CqCore.Logging
{
    public class EventData
    {
        [JsonProperty("id")]
        public string Id { get; set; } = Guid.NewGuid().ToString("N");
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("labels")]
        public Dictionary<string, string> Labels { get; set; } = new Dictionary<string, string>();
        [JsonProperty("time")]
        public DateTime Time { get; set; } = DateTime.Now;
    }
}
