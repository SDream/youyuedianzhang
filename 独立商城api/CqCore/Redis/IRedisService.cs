﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CqCore.Redis
{
    public interface IRedisService
    {
        Task<T> GetOrSet<T>(string key, Func<Task<T>> func, int hours = 24);
        Task<T> Get<T>(string key);
        Task<bool> Set<T>(string token, T obj, DateTime expressTime);
        Task<bool> Remove(string key);
        Task<bool> RemoveList(List<string> keys);
        Task<bool> AddItemToListT<T>(string key, T value);
        Task<T> GetLasterItemFromList<T>(string key);
    }
}
