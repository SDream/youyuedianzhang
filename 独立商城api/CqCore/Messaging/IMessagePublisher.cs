﻿using System;
using System.Threading.Tasks;

namespace CqCore.Messaging
{
    public interface IMessagePublisher : IDisposable
    {
        Task PublishAsync<TMessage>(string topic, TMessage message);
        //Next Version
        //Task PublishAsync<TEntity>(string topic, Message<TEntity> message);
    }

    public static class MessagePublisherExtensions
    {
        public static async Task PublishAsync<TMessage>(this IMessagePublisher context, TMessage message)
        {
            await context.PublishAsync<TMessage>(typeof(TMessage).FullName.ToLower(), message);
        }
    }
}
