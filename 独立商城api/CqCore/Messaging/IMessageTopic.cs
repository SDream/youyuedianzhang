﻿namespace CqCore.Messaging
{
    public interface IMessageTopic
    {
        string Topic { get; }
    }
}
