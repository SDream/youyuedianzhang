﻿using SuperShopInfrastructure.Models.Question;
using SuperShopRepository;
using SuperShopService.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly QuestionRepository questionRepository;

        public QuestionService(QuestionRepository questionRepository)
        {
            this.questionRepository = questionRepository;
        }

        public async Task Insert(CreateQuestionCommand model)
        {
            await questionRepository.Insert(model);
        }
    }
}
