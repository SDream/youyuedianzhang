﻿using CqCore.Commands;
using SuperShopInfrastructure.Models.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IUserService
    {
        Task<CommandResult<WxUserLoginViewModel>> WxUserLoginOrRegister(WxUserLoginCommand command);
    }
}
