﻿using SuperShopInfrastructure.Models.ShopApp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IShopAppService
    {
        Task<ShopAppViewModel> GetShopAppModel(int id);
        Task<string> GetPayKeyByAppid(string appid);
    }
}
