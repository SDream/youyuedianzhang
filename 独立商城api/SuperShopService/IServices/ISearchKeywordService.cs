﻿using SuperShopInfrastructure.Models.SearchKeyword;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface ISearchKeywordService
    {
        Task<List<SearchWordViewModel>> GetList(int said);
    }
}
