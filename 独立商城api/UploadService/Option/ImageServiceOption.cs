﻿namespace UploadService.Option
{
    public class ImageServiceOption
    {
        /// <summary>
        /// 当前项目站点图片虚拟目录地址
        /// </summary>
        public string DomainImageHost { get; set; }
        /// <summary>
        /// 图片服务器地址
        /// </summary>
        public string ImgHost { get; set; }
        /// <summary>
        /// 图片上传服务地址
        /// </summary>
        public string UploadHost { get; set; }

        /// <summary>
        /// 站点图片存储路径
        /// </summary>
        public string FileUploadPath { get; set; }

        public string FileUploadFolderNameTemp { get; set; } = "Temp";
    }
}
