﻿using CqCore.Extensions;
using CqCore.Logging;
using Microsoft.Extensions.Logging.Abstractions.Internal;
using Newtonsoft.Json;
using NLog;

namespace NLogService
{
    public class NLogRecorder<T> : ILogRecorder<T>
    {
        readonly ILogger logger;

        public NLogRecorder()
        {
            this.logger = LogManager.LogFactory.GetLogger(TypeNameHelper.GetTypeDisplayName(typeof(T)));
        }

        public void Debug(EventData data)
        {
            var json = JsonConvert.SerializeObject(data, JsonExtensions.DefaultSettings);
            logger.Debug(json);
        }

        public void Error(EventData data)
        {
            var json = JsonConvert.SerializeObject(data, JsonExtensions.DefaultSettings);
            logger.Error(json);
        }

        public void Fatal(EventData data)
        {
            var json = JsonConvert.SerializeObject(data, JsonExtensions.DefaultSettings);
            logger.Fatal(json);
        }

        public void Info(EventData data)
        {
            var json = JsonConvert.SerializeObject(data, JsonExtensions.DefaultSettings);
            logger.Info(json);
        }

        public void Trace(EventData data)
        {
            var json = JsonConvert.SerializeObject(data, JsonExtensions.DefaultSettings);
            logger.Trace(json);
        }

        public void Warn(EventData data)
        {
            var json = JsonConvert.SerializeObject(data, JsonExtensions.DefaultSettings);
            logger.Warn(json);
        }

        public void Dispose()
        {
        }
    }
}
