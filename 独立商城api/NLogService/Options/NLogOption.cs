﻿namespace NLogService.Options
{
    public class NLogOption
    {
        public string ConfigPath { get; set; }
        public string DataPath { get; set; }
        public string ModuleName { get; set; }
        public string NodeName { get; set; }
    }
}
