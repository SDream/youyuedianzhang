﻿using CqCore.DB;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class AreaRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public AreaRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetArea(int parentId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@parentId",parentId)
            };
            var sql = "select * from C_Areas where Parent_Id=@parentId";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
        public async Task<DataSet> GetAllArea()
        {
            var sql = "select * from C_Areas";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, null);
        }
    }
}
