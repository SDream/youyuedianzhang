﻿using CqCore.DB;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Comment;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class CommentRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public CommentRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<int> Create(CreateCommentCommand command)
        {
            SqlParameter rtn_err = await sqlService.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@OrderId", command.OrderId),
                new SqlParameter("@Spid",command.Spid.ToInt()),
                new SqlParameter("@UserId",command.UserId.ToInt()),
                new SqlParameter("@Said",command.Said.ToInt()),
                new SqlParameter("@CommentLevel ",command.CommentLevel),
                new SqlParameter("@CommentMsg",command.CommentMsg),
                rtn_err
            };
            await sqlService.ExecuteNonQuery(CommandType.StoredProcedure, "add_comment", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public async Task<DataSet> QueryComments(QueryCommentCriteria criteria)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@offset",criteria.OffSet),
                new SqlParameter("@size",criteria.Size),
                 new SqlParameter("@productId",criteria.ProductId.ToInt())
            };
            return await sqlService.ExecuteDataset(CommandType.StoredProcedure, "api_getcommentpagedlist", parms);
        }

        public async Task<DataSet> GetCounts(int productId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@productId",productId)
            };
            var sql = "select COUNT(Id) as totalCount from C_Comment where ProductId=@productId and IsDel = 0";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
    }
}
