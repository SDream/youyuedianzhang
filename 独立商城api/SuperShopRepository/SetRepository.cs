﻿using CqCore.DB;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class SetRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public SetRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetShippingTemplateModel(int id)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            //deleted
            var sql = "select * from C_ShippingTemplates where Id=@id;select* from C_ShippingTemplateItem where ShippingTemplateId = @id and IsDel=0";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
    }
}
