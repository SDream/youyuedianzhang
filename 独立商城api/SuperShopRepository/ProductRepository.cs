﻿using CqCore.DB;
using SuperShopInfrastructure.Models.Product;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class ProductRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public ProductRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetMenuList(int shopAdminId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId", shopAdminId)
            };
            var sql = "select Id,MenuName,ImgPath,IsAll from C_Menu where OptionStatus=1 and IsDel=0 and ShopAdminId=@shopAdminId order by Sorft";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
        public async Task<DataSet> GetProductList(QueryProductListCriteria criteria)
        {
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = await sqlService.CreateSb();
            if (criteria.MenuId != 0)
            {
                sb.Append(" and p.MenuId = @MenuId");
                parms.Add(new SqlParameter("@MenuId", criteria.MenuId));
            }
            if (!string.IsNullOrEmpty(criteria.keyWord))
            {
                sb.Append(" and p.ProductName Like '%' + @ProductName + '%'");
                parms.Add(new SqlParameter("@ProductName", criteria.keyWord));
            }
            parms.Add(new SqlParameter("@Spid", criteria.Spid));


            var sql = string.Format("select top({0}) * from (select ROW_NUMBER() over(order by p.Sorft asc)as rownum, p.Id,p.ProductName,p.Brand,p.Title,(select ImagePath from C_ProductImg where ProductId=p.Id and IsDel=0 and IsDefault=1) as ImgPath,(select top(1) SallPrice from C_Product_Sku where ProductId=p.Id and IsDel=0) as SallPrice,p.ShopAdminId from C_Product p  where  p.OptionStatus=1 and p.IsDel=0 and p.ShopAdminId=(select ShopAdminId from C_ShopApp where Id=@Spid) and {1} ) tt where tt.rownum>{2}", criteria.Size, sb.ToString(), criteria.Size * criteria.OffSet);
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }



        public async Task<DataSet> GetProduct(int id)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select p.*,(select top(1) SallPrice from C_Product_Sku where ProductId=p.Id and IsDel=0 order by SallPrice) as MinAmount,(select top(1) SallPrice from C_Product_Sku where ProductId=p.Id and IsDel=0 order by SallPrice desc) as MaxAmount from C_Product p where p.Id = @id;select * from C_ProductImg where ProductId=@id and IsDel=0 order by IsDefault desc";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<DataSet> GetCreateOrder_CheckItemDs(string skuids)
        {
            var sql = "select ps.Id as ProductSkuId, ps.StockNum,ps.IsDel,p.OptionStatus as Product_OptionStatus,ps.IsDel as Product_IsDel,p.ProductName,ps.ProppetyCombineName,ps.SallPrice,ps.SkuCode,ps.PropetyCombineId,ps.Weight from C_Product_Sku  ps with(nolock) left join C_Product  p with(nolock) on ps.ProductId=p.Id where ps.Id in(" + skuids + ")";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, null);
        }

        public async Task<DataSet> SpecialPlaceList(int shopAdminId,int type)
        {
            SqlParameter[] parms =
           {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@specialType",type)
            };
            var sql = "select s.*,m.MenuName from C_SpecialPlace s left join C_Menu m on s.MenuId=m.Id where s.ShopAdminId = @shopAdminId and SpecialType=@specialType and s.IsDel = 0 order by Sorft ,Id desc";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<DataSet> GetHomeData(int shopAppId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAppId", shopAppId)
            };
            return await sqlService.ExecuteDataset(CommandType.StoredProcedure, "GetHomeData", parms);
        }

        public async Task<DataSet> GetProductSkuDs(int shopAdminId, string productId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@productId",productId)
            };
            var sb = new StringBuilder();
            sb.Append("select * from C_Product_Sku where ProductId= @productId and ShopAdminId =@shopAdminId and SkuType=1 and IsDel=0;");

            sb.Append("select CurrentSkuType from C_Product where Id=@productId and ShopAdminId = @shopAdminId;");

            return await sqlService.ExecuteDataset(CommandType.Text, sb.ToString(), parms);
        }

        public async Task<DataSet> GetProductMoreSkuDs(int shopAdminId, string productId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@productId",productId)
            };
            var sb = new StringBuilder();
            sb.Append("select * from C_Product_Sku where ProductId= @productId and ShopAdminId =@shopAdminId and SkuType=2 and IsDel=0;");

            sb.Append("select * from C_Sall_Propety where ProductId=@productId and ShopAdminId=@shopAdminId and IsDel=0;");

            sb.Append("select* from C_Sall_Propety_Value where ProductId = @productId and IsDel = 0");
            return await sqlService.ExecuteDataset(CommandType.Text, sb.ToString(), parms);
        }
    }
}
