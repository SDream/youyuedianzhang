﻿(function () {
    //商城组件详细
    var CommentArr = [
        {
            Name: "promise",
            Template: '<div class="cq_block isComment" itemName="promise"><div class="service_bar"><span><i class="danbao"></i>官方担保</span><span><i class="_shouhou"></i>售后无忧</span><span><i class="_fahuo"></i>极速发货</span></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "承诺"
                }
            ]
        },
        {
            Name: "search",
            Template: '<div class="cq_block isComment"  itemName="search"><div class="search-box"><div class= "search-input font_26r" ><input id="searchInput" placeholder="请输入关键字进行搜索" /></div><div class="search-btn" ><img src="/images/search.png" /></div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "搜索"
                }
            ]
        },
        {
            Name: "keyword",
            Template: '<div class="cq_block isComment" itemName="keyword"><div class="hotSearch"><div  v-for="item in words" :data-hot="item.KeyWord" class="hotspan" > {{item.KeyWord}}</div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "搜索词"
                },
                {
                    Name: "数据源",
                    Type: "data"
                }
            ]
        },
        {
            Name: "banner",
            Template: '<div class="cq_block isComment" itemName="banner"><div class="swiper-container swiper-banner"><div class="swiper-wrapper" ><div v-for="item in banners" class="swiper-slide"><a href="javascript:void(0);"><img :src="item.ImgPath" /></a></div></div><div class="swiper-pagination"></div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "轮播"
                },
                {
                    Name: "数据源",
                    Type: "data"
                }
            ]
        },
        {
            Name: "head",
            Template: '<div class="header isComment" itemName="head" style="margin-bottom: 50px;" ><div class="search-box"><div class="search-input font_26r"><input id="searchInput" placeholder="请输入关键字进行搜索" /></div><div class="search-btn"><img src="/images/search.png" /></div></div ><div class="hotSearch"><div v-for="item in words" class="hotspan">{{ item.KeyWord }}</div></div ><div class="swiper-container swiper-banner"><div class="swiper-wrapper"><div v-for="item in banners" class="swiper-slide"><a href="javascript:void(0);"><img :src="item.ImgPath" /></a></div></div><div class="swiper-pagination"></div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "顶部组合"
                },
                {
                    Name: "搜索词",
                    Type: "data"
                },
                {
                    Name: "轮播图",
                    Type: "data"
                },
                {
                    Name: "背景颜色",
                    Type: "backgroundColor"
                }
            ]
        },
        {
            Name: "menu",
            Template: ' <div class="cq_block isComment" itemName="menu"><div class= "menuList" ><div class="menuDiv" v-for="item in menus"><div class="menuItem" style=""><img class="menuIcon" :src="item.ImgPath" /></div><div class="menuItemText">{{ item.MenuName }}</div></div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "分类目录"
                },
                {
                    Name: "数据源",
                    Type: "data"
                },
                {
                    Name: "背景颜色",
                    Type: "backgroundColor"
                }
            ]
        },
        {
            Name: "productSpecial",
            Template: '<div class="cq_block isComment" itemName="productSpecial"><div v-for="item in specialPlace" ><div hover-class="none"  style="text-align: center;"><img class="zhuanchang" :src="item.ImgPath" /></div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "商品专场"
                },
                {
                    Name: "数据源",
                    Type: "data"
                }
            ]
        },
        {
            Name: "productArea",
            Template: '<div class="isComment" itemName="productArea"><div class="line"><span>热门专区</span></div><div class="cq_block"><div class="hotBlock"><div class="hotBody"><div v-for="item in specialArea" class="hotBodyDiv"><a href="javascript:void(0);"><div class="hotBodyItem"><img class="hotBodyItemImg" :src="item.ImgPath" /><div class="hotText">{{ item.PlaceName }}</div></div></a></div></div></div></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "商品专区"
                },
                {
                    Name: "数据源",
                    Type: "data"
                }
            ]
        },
        {
            Name: "productHot",
            Template: '<div class="isComment" itemName="productHot" ><div class="line"><span>热门商品</span></div><div class="product_div" ><a v-for="item in hots" href="javascript:void(0);" class="cm-pic-nav"><div class="product_item"><div class="product_head"><img class="product_img" :src="item.ImgPath" /></div><div class="product_body"><div class="product_title">{{ item.Brand }} {{ item.ProductName }}</div><div class="product_span"><div class="product_amount">￥{{ item.SallPrice }}</div><div class="product_newLevel">已售3211</div></div></div></div></a></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "热门商品"
                },
                {
                    Name: "数据源",
                    Type: "data"
                }
            ]
        },
        {
            Name: "productCommand",
            Template: '<div class="isComment" itemName="productCommand"><div class="line"><span>最新推荐</span></div ><div class="product_div" ><a v-for="item in commands" href="javascript:void(0);" hover-class="none"><div class="product_item"><div class="product_head"><img class="product_img" :src="item.ImgPath" /></div><div class="product_body"><div class="product_title">{{ item.Brand }} {{ item.ProductName }}</div><div class="product_span"><div class="product_amount_new">￥{{ item.SallPrice }}</div><div class="list-item-line"></div></div></div></div></a></div></div>',
            Propertys: [
                {
                    Name: "组件名称",
                    Type: "string",
                    NameValue: "推荐商品"
                },
                {
                    Name: "数据源",
                    Type: "data"
                }
            ]
        }

    ];

    CommentManager.CommentNames = [
        "promise",
        "search",
        "keyword",
        "banner",
        "menu",
        "productSpecial",
        "productArea",
        "productHot",
        "productCommand",
        "head"
    ]

    CommentManager.ShopComment = {
        Get: function (name) {
            return CommentManager.ShopComment.FindByName(name);
        },
        FindByName: function (name) {
            var res = undefined;
            for (var i = 0; i < CommentArr.length; i++) {
                if (CommentArr[i].Name == name) {
                    res = CommentArr[i];
                    break;
                }
            }
            return res;
        }
    }


})();