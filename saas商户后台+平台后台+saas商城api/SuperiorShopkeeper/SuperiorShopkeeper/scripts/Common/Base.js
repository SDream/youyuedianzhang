﻿RequestManager = {};
ToolManager = {};
superior = {};
superior.ui = {};
superior.ui.control = {};
Map = {};
DataSet = {};
CommentManager = {};
Array.prototype.remove = function (val) {
    var index = this.indexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Array.prototype.diff = function (a) {
    return this.filter(
        function (i) {
            return a.indexOf(i) < 0;
        });

};

