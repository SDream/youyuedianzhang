﻿(function () {
    RechargeListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }, OrderStatus: 999, AppType: 999, SallType :999};
    RechargeListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;
                laydate.render({
                    elem: '#search_BeginTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
                laydate.render({
                    elem: '#search_EndTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
            });
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, RechargeListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },
        Search: function () {

            _searchCriteria.RechargeNo = $("#search_RechargeNo").val();
            _searchCriteria.AppType = $("#search_AppType").val();
            _searchCriteria.OrderStatus = $.trim($("#search_OrderStatus").val());
            _searchCriteria.SallType = $("#search_SallType").val();
            _searchCriteria.BeginTime = $("#search_BeginTime").val();
            _searchCriteria.EndTime = $("#search_EndTime").val();
            RechargeListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/ShopApp/RechargeList";
            
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ShopApp/RechargeList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()