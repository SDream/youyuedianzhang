﻿
(function () {
    ShopAdminClass = {};
    ShopAdminClass.Instance = {
        Init: function () {
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(add)', ShopAdminClass.Instance.Edit);
            });
            //-----end
        },
        Edit: function () {
            var text_contact = $("#text_contact").val();            
            var text_qq = $("#text_qq").val();
            var text_wx = $("#text_wx").val();
            var area_introduce = $("#area_introduce").val();

            var index = layer.load(1);
            RequestManager.Ajax.Post("/ShopAdmin/EditShopAdminInfo", {
                "Introduce": area_introduce,
                "QqCode": text_qq,
                "WxCode": text_wx,
                "Contact": text_contact,

            }, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("修改成功!");
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()