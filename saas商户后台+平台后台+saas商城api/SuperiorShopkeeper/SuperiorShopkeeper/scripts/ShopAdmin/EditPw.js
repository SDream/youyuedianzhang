﻿
(function () {
    EditPwClass = {};
    EditPwClass.Instance = {
        Init: function () {
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(add)', EditPwClass.Instance.Edit);
            });
            //-----end
        },
        Edit: function () {
            var text_oldpassword = $("#text_oldpassword").val();
            var text_password = $("#text_password").val();
            var reset_password = $("#reset_password").val();
           

            var index = layer.load(1);
            RequestManager.Ajax.Post("/ShopAdmin/EditPw", {
                "OldPassWord": text_oldpassword,
                "PassWord": text_password,
                "ResetPassWord": reset_password
            }, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("修改成功!");
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()