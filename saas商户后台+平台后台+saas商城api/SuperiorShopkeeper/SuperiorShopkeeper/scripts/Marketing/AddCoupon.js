﻿(function () {
    AddCouponClass = {};

    AddCouponClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
                //自定义验证规则
                form.verify({
                    //验证大于0得正整数规则
                    greaterThanZero: function (value, item) {
                        var r = parseFloat(value);
                        if (r <= 0)
                            return "必须大于0的数字";
                    },
                    //验证金额
                    isMoney: function (value, item) {//这条规则0也会通过，不知道咋改、
                        var pattern = /(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)/;
                        if (!pattern.test(value))
                            return '金额格式错误,必须大于0并且保留两位小数';
                    }
                });
                form.on('radio(choseType)', function (data) {
                    var value = data.elem.value;
                    switch (value) {
                        case '1':
                            $("#text_delAmount").parent().parent().show();
                            $("#text_discount").parent().parent().hide();
                            break;
                        case '2':
                            $("#text_delAmount").parent().parent().hide();
                            $("#text_discount").parent().parent().show();
                            break;
                    }
                });
                //验证提交
                form.on('submit(add)', AddCouponClass.Instance.Submit);
            });
            
        },
        
        Submit: function () {
            var model = {};
            var text_couponName = $.trim($("#text_couponName").val());
            var _type = "";
            $("input[name=text_status]").each(function (index, item) {
                if (item.checked)
                    _type = item.value;
            });
            var text_delAmount = $.trim($("#text_delAmount").val());
            var text_discount = $.trim($("#text_discount").val());
            var text_minOrderAmount = $.trim($("#text_minOrderAmount").val());
            var text_days = $.trim($("#text_days").val());
            var text_totalCount = $.trim($("#text_totalCount").val());
            if (_type == "1") {
                text_discount = 0;
            } else if (_type == "2") {
                text_delAmount = 0;
            }
            model.CouponName = text_couponName;
            model.CouponType = _type;
            model.DelAmount = text_delAmount;
            model.Discount = text_discount;
            model.MinOrderAmount = text_minOrderAmount;
            model.Days = text_days;
            model.TotalCount = text_totalCount;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Marketing/AddCoupon", model, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("添加成功");
                } else {
                    layer.alert(data.Message);
                }
            })
        }
      
    };
})();
