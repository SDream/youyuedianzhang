﻿using Redis;
using SuperiorCommon;
using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperiorShopkeeper
{
    public class ViewBagManager
    {
        public static bool IsCheck(string id, List<string> ids)
        {
            var res = false;
            foreach (var item in ids)
            {
                if (item == id)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }
        /// <summary>
        /// 判断当前选中的是否是全国
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="areas"></param>
        /// <returns></returns>
        public static bool IsAllArea(List<string> ids, List<AreaModel> areas)
        {
            var res = true;
            foreach (var item in areas)
            {
                if (!ids.Contains(item.Area_Code.ToString()))
                {
                    res = false;
                    break;
                }
            }
            return res;
        }
        /// <summary>
        /// 将省ID数组，转换成省字符串
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="areas"></param>
        /// <returns></returns>
        public static string GetAreaNameStr(List<string> ids, List<AreaModel> areas)
        {
            var res = "";
            if (IsAllArea(ids, areas))
            {
                res = "全国";
            }
            else
            {
                foreach (var id in ids)
                {
                    res += areas.Where(i=>i.Area_Code==id.ToInt()).FirstOrDefault().Area_Name+",";
                }
            }
            if (res.Contains(","))
                res.Remove(res.Length, 1);
            return res;
        }
        /// <summary>
        /// 获取当前授权商城的预授权码
        /// </summary>
        /// <param name="spid"></param>
        /// <returns></returns>
        public static string GetPre_auth_code(string spid)
        {
            //先从缓存获取
            var token = RedisManager.Get<string>("Component_token");
            if (token == null)
            {
                try
                {
                    var componentVerifyTicket = RedisManager.Get<string>("ComponentVerifyTicket");
                    if (componentVerifyTicket == null)
                    {
                        LogManger.Instance.WriteLog("等待微信推送ComponentVerifyTicket");
                        return "";
                    }
                    token = InterfaceApi.Component_token(componentVerifyTicket.ToString()).component_access_token;
                    //缓存1.5小时
                    RedisManager.Set<string>("Component_token", token, DateTime.Now.AddMinutes(90));
                }
                catch (Exception ex)
                {

                    LogManger.Instance.WriteLog("获取第三方平台token：" + ex.ToString());
                    return "";
                }
            }
            var _pre_auth_code = RedisManager.Get<string>(spid+"_pre_auth_code");
            if (_pre_auth_code == null)
            {
                _pre_auth_code = InterfaceApi.Create_preauthcode(token.ToString()).pre_auth_code;
                RedisManager.Set<string>(spid + "_pre_auth_code", _pre_auth_code, DateTime.Now.AddMinutes(10));
            }
            return _pre_auth_code;
            //Response.Write("success");
        }
    }
}