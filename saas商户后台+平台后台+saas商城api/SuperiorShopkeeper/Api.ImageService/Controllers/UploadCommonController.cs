﻿using SuperiorCommon;
using System;
using System.IO;
using System.Web;
using System.Web.Http;

namespace Api.ImageService.Controllers
{
    public class UploadCommonController : ApiController
    {
        // GET: UploadCommon

        [HttpGet]
        public string Test()
        {
            return "aaaaaa";
        }

        [HttpPost]
        public string Upload()
        {
            try
            {
                var uploadType = HttpContext.Current.Request["uploadType"];
                var fileUploader =UploadService.GetUploadInstance(uploadType);
                HttpPostedFile file = System.Web.HttpContext.Current.Request.Files.Get(0);
                var imageFullFileName = Path.Combine(fileUploader.PhysicalPath, Path.GetFileName(file.FileName));
                file.SaveAs(imageFullFileName);
                return "1";
            }
            catch (Exception ex)
            {
                LogManger.Instance.WriteLog(ex.ToString());
                return "0";
            }
            
        }

       

       
    }
}