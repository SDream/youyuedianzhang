﻿using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace H5.SuperiorShop.Controllers
{
    [JsonException]
    public class DecorateController : BaseController
    {

        private readonly IProductService _IProductService;
        private readonly IShopAppService _IShopAppService;
        private readonly ISearchWordService _ISearchWordService;

        public DecorateController(IProductService IProductService, IShopAppService IShopAppService, ISearchWordService ISearchWordService)
        {
            _IProductService = IProductService;
            _IShopAppService = IShopAppService;
            _ISearchWordService = ISearchWordService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IProductService.Dispose();
            this._IShopAppService.Dispose();
            this._ISearchWordService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Decorate

        [CheckUrlFitler]
        [CheckShopStatus]
        public ActionResult Index()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");

            return View();
        }

        /// <summary>
        /// 获取首页数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetHomeData()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<Api_HomeDataModel> fucc = delegate ()
            {
                return _IProductService.Api_GetHomeData(parms.Spid);
            };
            var res = DataIntegration.GetData<Api_HomeDataModel>("GetHomeData_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取轮播图数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetBanners()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetBanners(parms.Said);
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetBanners_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取热门商品数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetHotProducts()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetHotProducts(parms.Said);
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetHotProducts_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取推荐商品数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetCommandProducts()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetCommandProducts(parms.Said);
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetCommandProducts_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取目录
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetMenuList()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_MenuItemModel>> fuccMenu = delegate ()
            {
                return _IProductService.Api_GetMenuList(parms.Said);
            };
            var menuList = DataIntegration.GetData<List<Api_MenuItemModel>>("GetMenuList_" + parms.Said, fuccMenu);
            return Success(menuList);
        }

        /// <summary>
        /// 获取搜索词列表
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetWords(string said)
        {
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<List<SearchWordModel>> fucc = delegate ()
            {
                return _ISearchWordService.GetList(_said.ToInt());
            };
            var res = DataIntegration.GetData<List<SearchWordModel>>("GetSearchKeyword_" + _said, fucc, 24);
            res = res.Where(p => p.OptionStatus == 1).ToList();
            return Success(res);
        }

        /// <summary>
        /// 获取首页专场/专区数据
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetSpecialPlaceData(string said, int type = 1)
        {
            string _shopAdminId = base.ValidataParms(said);
            if (_shopAdminId == null)
                return Error("非法参数");
            //读取商品主体
            Func<List<SpecialPlaceModel>> fucc = delegate ()
            {
                return _IProductService.SpecialPlaceList(_shopAdminId.ToInt(), type);
            };
            var res = DataIntegration.GetData<List<SpecialPlaceModel>>("SpecialPlaceList" + _shopAdminId + "_" + type, fucc);
            return Success(res);
        }
    }
}