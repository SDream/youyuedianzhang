﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]  
    [JsonException]
    public class ProductController : BaseController
    {
        private readonly IProductService _IProductService;
        private readonly ISetService _ISetService;
        private readonly ICommentService _ICommentService;

        public ProductController(IProductService IProductService, ISetService ISetService, ICommentService ICommentService)
        {
            _IProductService = IProductService;
            _ISetService = ISetService;
            _ICommentService = ICommentService;
        }
        protected override void Dispose(bool disposing)
        {
            this._IProductService.Dispose();
            this._ISetService.Dispose();
            this._ICommentService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 获取首页专场/专区数据
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetSpecialPlaceData(string said, int type = 1)
        {
            string _shopAdminId = base.ValidataParms(said);
            if (_shopAdminId == null)
                return Error("非法参数");
            //读取商品主体
            Func<List<SpecialPlaceModel>> fucc = delegate ()
            {
                return _IProductService.SpecialPlaceList(_shopAdminId.ToInt(),type);
            };
            var res = DataIntegration.GetData<List<SpecialPlaceModel>>("SpecialPlaceList" + _shopAdminId + "_" + type, fucc);
            return Success(res);
        }

       

        /// <summary>
        /// 获取分类
        /// </summary>
        /// <returns></returns>
        /// 
        [CheckUrlFitler]
        [CheckShopStatus]
        public ActionResult MenuList(int menuid = 0)
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_MenuItemModel>> fuccMenu = delegate ()
            {
                return _IProductService.Api_GetMenuList(parms.Said);
            };
            var menuList = DataIntegration.GetData<List<Api_MenuItemModel>>("GetMenuList_" + parms.Said, fuccMenu);
            return View(menuList);
        }


        /// <summary>
        /// 分类商品页面
        /// </summary>
        /// <param name="menuid"></param>
        /// <returns></returns>
        /// 
        [CheckUrlFitler]
        public ActionResult MenuProductList(int menuid = 0,string keyword = null)
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            var criteria = new Api_ProductListCriteria()
            {
                Spid = parms.Spid.ToString(),
                OffSet = 0,
                Size = 10,
                MenuId = menuid,
                keyWord = keyword==null?"":keyword
            };
            var res = GetPagedListForMenuid(criteria);
            return View(res);
        }
        /// <summary>
        /// 搜索或分页获取分类下商品列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public JsonResult MenuProductList(Api_ProductListCriteria criteria)
        {
            criteria.Spid = base.ValidataParms(criteria.Spid);
            var res = GetPagedListForMenuid(criteria);
            return Success(res);
        }

        private List<Api_ProductItem> GetPagedListForMenuid(Api_ProductListCriteria criteria)
        {
            List<Api_ProductItem> res = null;
            if (string.IsNullOrEmpty(criteria.keyWord) && criteria.MenuId == 0)
            {
                Func<List<Api_ProductItem>> fucc = delegate ()
                {
                    return _IProductService.Api_GetProductList(criteria);
                };
                res = DataIntegration.GetData<List<Api_ProductItem>>("GetProductList_" + criteria.Spid + "_Offset_" + criteria.OffSet, fucc, 1);
            }
            else
            {
                res = _IProductService.Api_GetProductList(criteria);
            }
            return res;
        }
        /// <summary>
        ///商品详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [CheckUrlFitler]
        public ActionResult Detail(string id)
            {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            string _id = base.ValidataParms(id);
            if (_id == null)
                return Error("非法参数");
            //读取商品主体
            Func<Api_ProductDetailModel> fucc = delegate ()
            {
                return _IProductService.Api_GetProduct(_id.ToInt());
            };
            var res = DataIntegration.GetData<Api_ProductDetailModel>("GetProductDetail_" + _id, fucc);
            //读运费
            Func<ShippingTemplateModel> fucc1 = delegate ()
            {
                return _ISetService.GetShippingTemplateModel(res.ShipTemplateId);
            };
            var shipmodel = DataIntegration.GetData<ShippingTemplateModel>("ShippingTemplateModel_" + res.ShipTemplateId, fucc1);
            //取出运费所有区域的子运费模板的最大金额和最小金额
            var _minAmount = shipmodel.Items.Min(p => p.FirstAmount);
            var _maxAmount = shipmodel.Items.Max(p => p.FirstAmount);
            res.ShipAmount = _minAmount + " - " + _maxAmount;
            //根据商品主体的sku类型，读取商品的sku数据
            if (res.CurrentSkuType == 1)
            {
                res.SkuManagerModel = _IProductService.GetProductSkuManagerModel(parms.Said, _id);//由于库存原因，商品SKU不走redis
            }
            else if (res.CurrentSkuType == 2)
            {
                res.SkuManagerModel = _IProductService.GetProductSkuManagerModel_AJAX(parms.Said, _id);
            }
            ViewBag.Title = res.ProductName;
            ViewBag.CartCount = CartManager.Instance.Count("Cart_" + parms.Spid + "_" + UserContext.DeUserId);
            var commentCount = _ICommentService.GetCounts(_id.ToInt());
            ViewBag.CommentCount = commentCount;
            return View(res);
        }
    }
}