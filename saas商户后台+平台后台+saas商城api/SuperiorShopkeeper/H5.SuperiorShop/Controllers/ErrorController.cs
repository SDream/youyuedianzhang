﻿using System.Web.Mvc;
using SuperiorCommon;

namespace H5.SuperiorShop.Controllers
{
    public class ErrorController : BaseController
    {
        // GET: Error
        public ActionResult Index(string msg)
        {
            ViewBag.Msg = msg;
            return View();
        }

        public JsonResult Write(string error)
        {
            LogManger.Instance.WriteLog(error);
            return Success(true);
        }
    }
}