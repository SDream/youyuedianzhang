﻿(function () {
    AddClass = {};
    var _level = 5;
    var islock = false;//防止同时提交两次数据
    AddClass.Instance = {
        Init: function () {
            $(document).on('click', '#choseLevel li', this.ChoseLevel);
            $(document).on('click', '#btn_submit', this.Submit);
        },
        ChoseLevel: function () {
            _level = parseInt($(this).attr('level'));
            $("#choseLevel li").each(function () {
                if (parseInt($(this).attr('level')) <= _level)
                    $(this).addClass("on");
                else
                    $(this).removeClass("on");
            })
        },
        Submit: function () {
            if (islock)
                return false;
            islock = true;
            var _spid = ToolManager.Common.UrlParms("spid");
            var _said = ToolManager.Common.UrlParms("said");
            var _orderId = ToolManager.Common.UrlParms("orderId");
            var commentMsg = $("#commentMsg").val();
            if ($.trim(commentMsg) == "") {
                
                islock = false;
                return false;
            }
                
            var model = {};
            model.OrderId = _orderId;
            model.Spid = _spid;
            model.Said = _said;
            model.CommentLevel = _level;
            model.CommentMsg = commentMsg;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Comment/Add?spid=" + _spid, model, true, function (data) {
                layer.close(index);
                islock = false;
                if (data.IsSuccess) {
                    layer.msg("发表成功!");
                    
                } else {
                    layer.msg(data.Message);
                }
            })
        }
    }
})()