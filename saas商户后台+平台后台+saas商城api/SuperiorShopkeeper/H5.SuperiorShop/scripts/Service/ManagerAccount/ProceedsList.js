﻿(function () {
    var isScrollLoad = false;
    var winH = $(window).height(); //页面可视区域高度
    var lastRequestCount = 0;
    var pageIndex = 1;
    var pageSize = 10;
    ProceedsListClasss = {};
    ProceedsListClasss.Instance = {
        Init: function () {
            lastRequestCount = $(".weui-content").attr("LoadRequstCount");
            $(window).scroll(ProceedsListClasss.Instance.ScrollHandler);
        },

        ScrollHandler: function () {
            if (isScrollLoad || lastRequestCount < pageSize) {
                return false;
            }

            var pageH = $(document.body).height();
            var scrollT = $(window).scrollTop(); //滚动条top
            var aa = (pageH - winH - scrollT) / winH;
            if (aa < 0.3) {//0.02是个参数
                isScrollLoad = true;
                ProceedsListClasss.Instance.Request();
            }
        },
        Request: function () {
            $(".weui-loadmore").show();
            $.get("/ManagerAccount/SearchProceedsList?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&r=" + new Date().getTime(), function (data) {
                $(".weui-loadmore").hide();
                if (data.IsSuccess) {
                    $(".weui-loadmore").hide();
                    lastRequestCount = data.Data.length;
                    if (data.Data.length > 0) {
                        pageIndex++;
                        var htmlArr = [];
                        data.Data.forEach((v) => {
                            htmlArr.push('<div class="weui-panel__ft">');
                            htmlArr.push('<div class="weui-cell weui-cell_access weui-cell_link">');
                            htmlArr.push(' <div class="weui-cell__bd">');
                            htmlArr.push(' <div>' + v.LoginName + '</div>');
                            if (v.AppType == 1) {
                                htmlArr.push('<div>小程序充值-' + v.Title + '</div>');
                            } else if (v.AppType == 2) {
                                htmlArr.push('<div>H5充值-' + v.Title + '</div>');
                            }
                            htmlArr.push(' <div>充值金额' + v.RechargeAmount + '</div>');
                            htmlArr.push('</div>');
                            htmlArr.push('<div class="weui_cell__ft">');
                            htmlArr.push('<div>' + v.CreateTime + '</div>');
                            htmlArr.push('<div>成功</div>');
                            htmlArr.push('<div>收益+' + v.AccountMangerAmount + '</div>');
                            htmlArr.push('</div>');
                            htmlArr.push('</div>');
                            htmlArr.push('</div>');
                        });
                        $("#div_list").append(htmlArr.join(''));
                    }
                    isScrollLoad = false;
                } else {
                    layer.alert(data.Message);
                }
            })

        },

    }
})();