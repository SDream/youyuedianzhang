﻿(function () {
    ManagerAccountIndexClass = {};
    var _amount;//提现金额
    var islock = false;//防止同时提交两次数据
    var isScrollLoad = false;
    var winH = $(window).height(); //页面可视区域高度
    var lastRequestCount = 0;
    var pageIndex = 1;
    var pageSize = 10;
    ManagerAccountIndexClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_tx', this.ShowPromt);
            lastRequestCount = $(".weui-content").attr("LoadRequstCount");
            $(window).scroll(ManagerAccountIndexClass.Instance.ScrollHandler);
            $(document).on('click', '#btn_see', this.See);
            $(document).on('click', '#btn_refresh', this.Reload);
        },
        Reload: function () {
            window.location.reload();
        },
        See: function () {
            layer.open({
                title:'推广说明',
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['280px', '240px'], //宽高
                content: '  记住您的推广人ID，您推广获取到的客户在注册商户平台时,在客户经理编号处填写您的ID,这个客户在以后的所有充值续费时,您都会获取充值/续费金额的20%收益。'
            });
           
        },
        ScrollHandler: function () {
            if (isScrollLoad || lastRequestCount < pageSize) {
                return false;
            }

            var pageH = $(document.body).height();
            var scrollT = $(window).scrollTop(); //滚动条top
            var aa = (pageH - winH - scrollT) / winH;
            if (aa < 0.3) {//0.02是个参数
                isScrollLoad = true;
                ManagerAccountIndexClass.Instance.Request();
            }
        },
        Request: function () {
            $(".weui-loadmore").show();
            $.get("/ManagerAccount/SearchCashOutList?pageIndex=" + pageIndex + "&pageSize=" + pageSize + "&r=" + new Date().getTime(), function (data) {
                $(".weui-loadmore").hide();
                if (data.IsSuccess) {
                    $(".weui-loadmore").hide();
                    lastRequestCount = data.Data.length;
                    if (data.Data.length > 0) {
                        pageIndex++;
                        var htmlArr = [];
                        data.Data.forEach((v) => {
                            htmlArr.push('<div class="weui-panel__ft">');
                            htmlArr.push('<div class="weui-cell weui-cell_access weui-cell_link">');
                            htmlArr.push(' <div class="weui-cell__bd">');
                            htmlArr.push('<div>单号'+v.OrderNo+'</div>');
                            if (v.OrderStatus == 0) {
                                htmlArr.push('<div>处理中</div>');
                            } else if (v.OrderStatus == 1) {
                                htmlArr.push(' <div>成功</div>');
                            } else if (v.OrderStatus == 2) {
                                htmlArr.push(' <div>失败</div>');
                            }
                         
                            htmlArr.push('</div>'); 
                            htmlArr.push('<div class="weui_cell__ft">');
                            htmlArr.push('<div>' + v.CreateTime + '</div>');
                            htmlArr.push('<div>-    ' + v.Amount + '</div>');
                            htmlArr.push('</div>');
                            htmlArr.push('</div>');
                            htmlArr.push('</div>');
                        });
                        $("#div_list").append(htmlArr.join(''));
                    }
                    isScrollLoad = false;
                } else {
                    layer.alert(data.Message);
                }
            })

        },
        ShowPromt: function () {
            layer.prompt({ title: '输入提现金额，并确认', formType: 0 }, function (data, index) {
                if (islock)
                    return false;
                islock = true;
                var totalAmount = $("#hideAmount").val();
                
                if (!ToolManager.Validata.IsMoney(data)) {
                    islock = false;
                    layer.msg("非法金额");
                    return false;
                }
                if (parseFloat(data) == 0) {
                    islock = false;
                    layer.msg("请输入大于0的金额");
                    return false;
                }
                if (parseFloat(data) > parseFloat(totalAmount)) {
                    islock = false;
                    layer.msg("不能超过可提现金额");
                    return false;
                }
                _amount = data;
                var model = {};
                model.Code = "111111";//模型和商户提现公用，那边要验证code，这里验证码给写死，
                model.Amount = _amount;
                var index1 = layer.load(1);
                RequestManager.Ajax.Post("/ManagerAccount/SubmitAmount", model, true, function (data) {
                    layer.close(index1);
                    islock = false;
                    if (data.IsSuccess) {
                        _amount = 0;
                        //layer.msg("提现成功,请注意微信查收,提现记录在商户后台可查");
                        layer.alert("尊敬的客户,您的提现申请已经成功,公司的企业付款正在申请中,暂时手动为您转账,将提现单号发送给公司运营微信wang324596966");
                        $("#div_amount").html("￥" + data.Data);
                        layer.close(index);
                    } else {
                        layer.msg(data.Message);
                        layer.close(index);
                    }
                })
                
            });
        },
        

   
    }

})()