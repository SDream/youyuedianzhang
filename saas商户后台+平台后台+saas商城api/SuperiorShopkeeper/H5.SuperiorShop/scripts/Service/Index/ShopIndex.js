﻿(function () {
    ShopIndexClass = {};
    var _amount;//提现金额
    var islock = false;//防止同时提交两次数据
    ShopIndexClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_tx', this.ShowTxDiv);
            $(document).on('click', '#btn_getcode', this.GetCode);
            $(document).on('click', '#btn_submit', this.Submit);
        },
        Submit:function(){
            if (islock)
                return false;
            islock = true;
            if (_amount == "") {
                layer.msg("提现金额为空");
                islock = false;
                return false;
            }
            var code = $("#txt_code").val();
            if (code == "") {
                layer.msg("请输入验证码");
                islock = false;
                return false;
            }
            var model = {};
            model.Code = code;
            model.Amount = _amount;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ServiceShop/SubmitAmount", model, true, function (data) {
                layer.close(index);
                islock = false;
                if (data.IsSuccess) {
                    _amount=0;
                    //layer.msg("提现成功,请注意微信查收,提现记录在商户后台可查");
                    layer.alert("尊敬的客户,您的提现申请已经成功,公司的企业付款正在申请中,暂时手动为您转账,请到商户后台提现管理中,将提现单号发送给公司运营微信wang324596966");
                    $("#div_amount").html("￥"+data.Data);
                } else {
                    layer.msg(data.Message);
                }
            })
        },
      
        ShowTxDiv: function () {
            layer.prompt({ title: '输入提现金额，并确认', formType: 0 }, function (data, index) {
                var totalAmount = $("#hideAmount").val();
                if (!ToolManager.Validata.IsMoney(data)) {
                    layer.msg("非法金额");
                    return false;
                }
                if (parseFloat(data) == 0) {
                    layer.msg("请输入大于0的金额");
                    return false;
                }
                if (parseFloat(data) > parseFloat(totalAmount)) {
                    layer.msg("不能超过可提现金额");
                    return false;
                }
                _amount = data;
                $("#showMsgDiv").popup();
                layer.close(index);
            });

            //$("#showMsgDiv").popup();
        },
        GetCode:function(){
            var phone = $("#hidePhone").val();
            $.get("/ServiceShop/GetCode?phone=" + phone + "&r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    $("#btn_getcode").attr("disabled","disabled");
                    ShopIndexClass.Instance.TimerMsg();
                } else {
                    layer.alert(data.Message);
                }
            })
        },
        TimerMsg: function () {
            var count = 60;
            var timer = setInterval(function () {

                if (count == 0) {
                    
                    $("#btn_getcode").attr("disabled", false).html("获取验证码");;
                    window.clearInterval(timer);
                    return false;
                }
                $("#btn_getcode").html(count+"秒后再次发送");
                count--;
            },1000)
        },
        
    }

})()