﻿using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SuperiorCommon;

namespace H5.SuperiorShop
{
    public class ViewHelper
    {
        /// <summary>
        /// 商品详情页面，加载时，多规格商品，获取当前默认的SKU模型
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static MoreSkuModel GetCurrentMoreSkuModel(ProductSkuManagerModel model)
        {
            var propetyCombineId = "";
            var propetyList = model.Product_Sall_PropetyModelList;
            for (int i = 0; i < propetyList.Count; i++)
            {
                if (i == propetyList.Count - 1)
                    propetyCombineId += propetyList[i].Product_Sall_Propety_ValueList[0].Id;
                else
                    propetyCombineId += propetyList[i].Product_Sall_Propety_ValueList[0].Id + "-";
            }
            MoreSkuModel sku = model.MoreSkuModelList.Where(p => p.PropetyCombineId == propetyCombineId).FirstOrDefault();
            return sku;

        }

        public static string ConvertJson(Api_ProductDetailModel model)
        {
            return model.ToJson();
        }
        public static string ConvertJson(List<Api_SureOrderModel> model)
        {
            return model.ToJson();
        }

        public static int CartCount()
        {
            int res = 0;
            try
            {
                string spid = SecretClass.DecryptQueryString(HttpContext.Current.Request["spid"]);
                string userid = UserContext.DeUserId;
                var key = "Cart_" + spid + "_" + userid;
                res = CartManager.Instance.Count(key);
            }
            catch (Exception ex)
            {

                LogManger.Instance.WriteLog("获取页面CartCount异常:"+ex.ToString());
            }
            return res;
            
        }

        public static string ConvertOrderStatus(int orderStatus)
        {
            var res = "";
            switch (orderStatus)
            {
                case 0:
                    res = "待付款";
                    break;
                case 1:
                    res = "成功(已发货)";
                    break;
                case 2:
                    res = "交易失败";
                    break;
                case 3:
                    res = "待发货";
                    break;
                case 4:
                    res = "申请售后";
                    break;
                   
            }
            return res;
        }
    }
}