﻿using SuperiorCommon;
using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Service.SuperiorShop.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //TempData["LayoutUserName"] = UserName;
            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// 解密参数验证。
        /// </summary>
        /// <param name="secret"></param>
        /// <returns></returns>
        protected virtual string ValidataParms(string secret)
        {
            var res = 0;
            if (int.TryParse(SecretClass.DecryptQueryString(secret), out res))
            {
                return res.ToString();
            }
            return null;
        }

        /// <summary>
        /// 返回请求成功消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="res"></param>
        /// <returns></returns>
        protected virtual JsonResult Success(object res)
        {
            var requestType = HttpContext.Request.RequestType;
            if (requestType == "GET")
                return Json(new ResultModel(true, string.Empty, res), JsonRequestBehavior.AllowGet);
            return Json(new ResultModel(true, string.Empty, res));
        }
        /// <summary>
        /// 返回请求失败的消息
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected virtual JsonResult Error(string msg)
        {
            var requestType = HttpContext.Request.RequestType;
            if (requestType == "GET")
                return Json(new ResultModel(false, msg), JsonRequestBehavior.AllowGet);
            return Json(new ResultModel(false, msg));
        }

        protected virtual string ErrorMsg()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //获取所有错误的Key
            List<string> Keys = ModelState.Keys.ToList();
            //获取每一个key对应的ModelStateDictionary
            foreach (var key in Keys)
            {
                var errors = ModelState[key].Errors.ToList();
                //将错误描述添加到sb中
                foreach (var error in errors)
                {
                    sb.Append(error.ErrorMessage + "| ");
                }
            }
            return sb.ToString();
        }
      
     
    }
}