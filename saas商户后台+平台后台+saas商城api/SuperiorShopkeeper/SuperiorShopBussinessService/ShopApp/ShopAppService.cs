﻿using SuperiorModel;
using System;
using System.Collections.Generic;
using SuperiorShopDataAccess;
using System.Data;
using SuperiorCommon;

namespace SuperiorShopBussinessService
{
    public class ShopAppService : IShopAppService
    {
        public void Dispose()
        {

        }
        public PagedSqlList<ShopAppModel> ShopAppList(ShopAppCriteria criteria)
        {
            var queryList = new List<ShopAppModel>();
            var totalCount = 0;
            var ds = ShopAppDataAccess.ShopAppList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ShopAppModel()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            CreateTime = dr["CreateTime"].ToString(),
                            UpdateTime = dr["UpdateTime"].ToString(),
                            ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            ServiceQQ = dr["ServiceQQ"].ToString(),
                            ServiceWx = dr["ServiceWx"].ToString(),
                            LoginName = dr["LoginName"].ToString(),
                            ServicePhone = dr["ServicePhone"].ToString(),
                            ShopName = dr["ShopName"].ToString(),
                            Version = dr["Version"].ToString(),
                            PayType = DataManager.GetRowValue_Int(dr["PayType"]),
                            SecretId = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            Remark = dr["Remark"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ShopAppModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public int Add(ShopAppModel model,out string msg)
        {
            var ret = ShopAppDataAccess.Add(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "当前小程序APPID已存在";
                    break;
                case 3:
                    msg = "当前店主账号下已存在店铺名称";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }
        public int Edit(ShopAppModel model,out string msg)
        {
            var ret = ShopAppDataAccess.Edit(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "小程序APPID已存在";
                    break;
                case 3:
                    msg = "当前店主账号下已存在店铺名称";
                    break;
                case 4:
                    msg = "当前店铺不存在";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public ShopAppModel GetShopAppModel(int id)
        {
            var res = new ShopAppModel();
            var ds = ShopAppDataAccess.GetShopAppModel(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.AppId = dr["AppId"].ToString();
                    res.AppSecret = dr["AppSecret"].ToString();
                    res.ShopAdminId  = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString());
                    res.PaymentId = dr["PaymentId"].ToString();
                    res.PaySecret = dr["PaySecret"].ToString();
                    res.AppType = Convert.ToInt32(dr["AppType"]);
                    res.ServiceQQ = dr["ServiceQQ"].ToString();
                    res.ServiceWx = dr["ServiceWx"].ToString();
                    res.Version = dr["Version"].ToString();
                    res.ServicePhone = dr["ServicePhone"].ToString();
                    res.PayType = DataManager.GetRowValue_Int(dr["PayType"]);
                    res.ShopName = dr["ShopName"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.SecretId = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.ProgrameExpireTime = DataManager.GetRowValue_DateTime(dr["ProgrameExpireTime"]);
                    res.H5ExpireTime = DataManager.GetRowValue_DateTime(dr["H5ExpireTime"]);
                }
            }
            return res;
        }

        public string GetPayKeyByAppid(string appid)
        {
            var res = "";
            var ds = ShopAppDataAccess.GetPaykeyByAppid(appid);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res = dr["PaySecret"].ToString();
                }
            }
            return res;
        }

        public void OptionShopAppStatus(int id, int option)
        {
            ShopAppDataAccess.OptionShopAppStatus(id,option);
        }
        public void UpdateVersion(int id, string version)
        {
            ShopAppDataAccess.UpdateVersion(id,version);
        }
        public bool IsExistShopByAdminId(int uid)
        {
            return ShopAppDataAccess.IsExistByUid(uid);
        }
    }
}
