﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IManagerAccountService : IService
    {

        PagedSqlList<ManagerAccountModel> SearchManagerAccountList(ManagerAccountListCriteria criteria);
        PagedSqlList<ManagerAccountOrderModel> SearchManagerAccountOrderList(ManagerAccountOrderListCriteria criteria);

        void OptionManagerAccount(int id, int option);
        int TransferAmount(int id,out string msg);
        void UpdateCashCoutOder(string orderno, int status);
        ManagerAccountModel Wx_Login(string openid, string headImgUrl, string nickName);
        ManagerAccountModel GetModel(int id);

        SubmitCashOutResultModel CreateCashOutOrder(int managerAccountId, decimal amount, string orderno, out string msg);
        int GetProceedsCount(int id);
        List<ServiceCashOutLogItem> SearchServiceCashOutLogItemList(int managerAccountId, int pageIndex, int pageSize);
        List<ServiceProceedsItem> SearchServiceProceedsItemList(int managerAccountId, int pageIndex, int pageSize);
    }






























































































































































}
