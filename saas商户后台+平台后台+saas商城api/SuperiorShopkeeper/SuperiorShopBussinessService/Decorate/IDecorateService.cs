﻿using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorShopBussinessService
{
    public interface IDecorateService: IService
    {
        DecorateModel GetDecorateModel(int shopAdminId);
        void CreateOrEditDecorate(DecorateModel model);
    }
}
