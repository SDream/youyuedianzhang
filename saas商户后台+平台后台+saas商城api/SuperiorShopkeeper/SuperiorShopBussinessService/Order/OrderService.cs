﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class OrderService : IOrderService
    {
        public PagedSqlList<OrderModel> Search(OrderCriteria criteria)
        {
            var queryList = new List<OrderModel>();
            var totalCount = 0;
            var ds = OrderDataAccess.Search(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new OrderModel()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            OrderNo = dr["OrderNo"].ToString(),
                            ShopName = dr["ShopName"].ToString(),
                            NickName = dr["NickName"].ToString(),
                            ServiceStatus = Convert.ToInt32(dr["ServiceStatus"]),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            RealityAmount = Convert.ToDecimal(dr["RealityAmount"]),
                            OrderStatus = Convert.ToInt32(dr["OrderStatus"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            LoginName = dr["LoginName"].ToString(),
                            FailReason = dr["FailReason"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<OrderModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public OrderDetailModel GetDetail(int orderid)
        {
            var res = new OrderDetailModel();
            var ds = OrderDataAccess.GetDetail(orderid);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.OrderNo = dr["OrderNo"].ToString();
                    res.ConsigneeName = dr["ConsigneeName"].ToString();
                    res.ConsigneePhone = dr["ConsigneePhone"].ToString();
                    res.ZipCode = dr["ZipCode"].ToString();
                    res.Address = dr["Address"].ToString();
                    res.PayOderNo = dr["PayOderNo"].ToString();
                    res.RealityAmount = dr["RealityAmount"].ToString();
                    res.Remark = dr["Remark"].ToString();
                    res.FailReason = dr["FailReason"].ToString();
                    res.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    res.PayTime = dr["PayTime"].ToString();
                    res.SendTime = dr["SendTime"].ToString();
                    res.ServiceTime = dr["ServiceTime"].ToString();
                    res.OrderStatus = Convert.ToInt32(dr["OrderStatus"]);
                    res.SendStatus = Convert.ToInt32(dr["SendStatus"]);
                    res.ServiceStatus = Convert.ToInt32(dr["ServiceStatus"]);
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.OrderAmount = DataManager.GetRowValue_Decimal(dr["OrderAmount"]);
                    res.CouponDelAmount = DataManager.GetRowValue_Decimal(dr["CouponDelAmount"]);
                    res.ShippingAmount = DataManager.GetRowValue_Decimal(dr["ShippingAmount"]);
                    res.CouponId = DataManager.GetRowValue_Int(dr["CouponId"]);
                    res.CouponName = dr["CouponName"].ToString();
                    res.DeliveryName = dr["DeliveryName"].ToString();
                    res.DeliveryNo = dr["DeliveryNo"].ToString();
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.OrderItems.Add(new OrderItem() {
                            ProductName = dr["ProductName"].ToString(),
                            ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                            SkuCode = dr["SkuCode"].ToString(),
                            BuyNum = Convert.ToInt32(dr["BuyNum"]),
                            SalePrice = Convert.ToDecimal(dr["SalePrice"])
                        });
                    }
                }

            }
            return res;
        }

        public void SendProduct(SendProductParms model)
        {
            OrderDataAccess.SendProduct(model);
        }
        public void UpdateDelivery(SendProductParms model)
        {
            OrderDataAccess.UpdateDelivery(model);
        }
        public void FinishService(int orderid)
        {
            OrderDataAccess.FinishService(orderid);
        }

        public Api_WeixinCreateOrderModel Program_CreateOrder_Cart(Api_CreateOrderCriteria model, out string msg)
        {
            int ret = 0;
            msg = "";
            var res = new Api_WeixinCreateOrderModel();
            var ds = OrderDataAccess.Program_CreateOrder_Cart(model, out ret);
            if (ret != 1)
            {
                switch (ret)
                {
                    case 2:
                        msg = "用户不存在";
                        break;
                    case 3:
                        msg = "地址不存在";
                        break;
                    case 4:
                        msg = "商户不存在";
                        break;
                    case 5:
                        msg = "执行事务出错";
                        break;
                    default:
                        msg = "下单异常";
                        break;

                }
            }
            else
            {
                if (DataManager.CheckDs(ds, 1))
                {
                    if (DataManager.CheckHasRow(ds.Tables[0]))
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        res.Appid = dr["Appid"].ToString();
                        res.OrderNumber = dr["OrderNumber"].ToString();
                        res.Paykey = dr["Paykey"].ToString();
                        res.Mch_id = dr["Mch_id"].ToString();
                        res.Openid = dr["Openid"].ToString();
                        res.TotalFee = double.Parse(dr["TotalFee"].ToString());
                        res.OrderID = int.Parse(dr["OrderID"].ToString());
                        res.PayType = Convert.ToInt32(dr["PayType"]);
                    }

                }
            }
            return res;
        }

        public int Program_WxNotice_Cart(string out_trade_no, int status, float total_fee, string transaction_id)
        {
            return OrderDataAccess.Program_WxNotice_Cart(out_trade_no, status, total_fee, transaction_id);
        }

        public Api_OrderDetail_InfoModel Api_GetDetail(int orderid)
        {
            var res = new Api_OrderDetail_InfoModel();
            var ds = OrderDataAccess.Api_GetDetail(orderid);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]) && DataManager.CheckHasRow(ds.Tables[1]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.ConsigneeName = dr["ConsigneeName"].ToString();
                    res.ConsigneePhone = dr["ConsigneePhone"].ToString();
                    res.Address = dr["Address"].ToString();
                    res.OrderNo = dr["OrderNo"].ToString();
                    res.Remark = dr["Remark"].ToString();
                    res.OrderAmount = Convert.ToDecimal(dr["OrderAmount"]);
                    res.CouponName = dr["CouponName"].ToString();
                    res.CreateTime = Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd HH:mm:ss");
                    res.CommentId = Convert.ToInt32(dr["CommentId"]);
                    res.PayTime = dr["PayTime"].ToString();
                    res.RealityAmount = Convert.ToDecimal(dr["RealityAmount"]);
                    res.CouponDelAmount = Convert.ToDecimal(dr["CouponDelAmount"]);
                    res.ShippingAmount = Convert.ToDecimal(dr["ShippingAmount"]);
                    res.OrderID = Convert.ToInt32(dr["Id"]);
                    res.DeliveryName = dr["DeliveryName"].ToString();
                    res.DeliveryNo = dr["DeliveryNo"].ToString();
                    foreach (DataRow dr_detail in ds.Tables[1].Rows)
                    {
                        res.Products.Add(new Api_OrderDetail_ProductItem() {
                            ProductId = SecretClass.EncryptQueryString(dr_detail["ProductId"].ToString()),
                            ProductName = dr_detail["ProductName"].ToString(),
                            ProppetyCombineName = dr_detail["ProppetyCombineName"].ToString(),
                            ImagePath = ConfigSettings.Instance.ImageUrl + dr_detail["ImagePath"].ToString(),
                            BuyNum = Convert.ToInt32(dr_detail["BuyNum"]),
                            SalePrice = Convert.ToDecimal(dr_detail["SalePrice"])
                        });
                    }
                }

            }
            return res;
        }

        public List<Api_OrderListModel> Api_GetOrderList(Api_OrderListCriteria criteria)
        {
            var queryList = new List<Api_OrderListModel>();
            var ds = OrderDataAccess.Api_GetOrderList(criteria);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new Api_OrderListModel()
                        {
                            OrderID = Convert.ToInt32(dr["Id"]),
                            ProductNum = Convert.ToInt32(dr["ProductNum"]),
                            OrderStatus = Convert.ToInt32(dr["OrderStatus"]),
                            BuyNum = Convert.ToInt32(dr["BuyNum"]),
                            OrderNo = dr["OrderNo"].ToString(),
                            ProductId = SecretClass.EncryptQueryString(dr["ProductId"].ToString()),

                            Said = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                            ProductName = dr["ProductName"].ToString(),
                            ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                            ImagePath = ConfigSettings.Instance.ImageUrl + dr["ImagePath"].ToString(),
                            RealityAmount = Convert.ToDecimal(dr["RealityAmount"]),
                            CommentId = Convert.ToInt32(dr["CommentId"]),
                            SalePrice = Convert.ToDecimal(dr["SalePrice"])
                        });
                    }
                }

            }
            return queryList;
        }

        public int Api_GetOrderStatus(int orderId)
        {
            var res = 0;
            var ds = OrderDataAccess.Api_GetOrderStatus(orderId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res = Convert.ToInt32(ds.Tables[0].Rows[0]["OrderStatus"]);
                }
            }
            return res;
        }

        public bool Api_ServiceOrder(int orderid, int userid)
        {
            return OrderDataAccess.Api_ServiceOrder(orderid,userid);
        }

        public Api_OrderCountModel Api_GetOrderCountDs(int userid)
        {
            var res = new Api_OrderCountModel();
            var ds = OrderDataAccess.Api_GetOrderCountDs(userid);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        switch (dr["OrderStatus"].ToString())
                        {
                            case "0":
                                res.WaitPayCount = Convert.ToInt32(dr["Num"]);
                                break;
                            case "1":
                                res.SendCount = Convert.ToInt32(dr["Num"]);
                                break;
                            case "2":
                                res.FailCount = Convert.ToInt32(dr["Num"]);
                                break;
                            case "3":
                                res.WaitSendCount = Convert.ToInt32(dr["Num"]);
                                break;
                            case "4":
                                res.ServiceCount = Convert.ToInt32(dr["Num"]);
                                break;
                        }
                    }
                }
            }
            return res;
        }

        public void Dispose()
        {

        }
    }
}
