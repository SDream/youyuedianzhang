﻿using System;
using System.Collections.Generic;
using System.Linq;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;
using SuperiorCommon;
using System.IO;

namespace SuperiorShopBussinessService
{
    public class ProductService : IProductService
    {

        public PagedSqlList<ProductModel> ProductList(ProductCriteria criteria)
        {
            var queryList = new List<ProductModel>();
            var totalCount = 0;
            var ds = ProductDataAccess.ProductList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ProductModel()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            CreateTime = dr["CreateTime"].ToString(),
                            ProductName = dr["ProductName"].ToString(),
                            LoginName = dr["LoginName"].ToString(),
                            ShopAdminId = DataManager.GetRowValue_Int(dr["ShopAdminId"]),
                            MenuId = Convert.ToInt32(dr["MenuId"]),
                            Brand = dr["Brand"].ToString(),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            Sorft = Convert.ToInt32(dr["Sorft"]),
                            Unit = dr["Unit"].ToString(),
                            MenuName = dr["MenuName"].ToString(),
                            ProductImg = ConfigSettings.Instance.ImageUrl + dr["ImagePath"].ToString(),
                            InitSallNum = Convert.ToInt32(dr["InitSallNum"]),
                            SallCount = Convert.ToInt32(dr["SallCount"])
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ProductModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public PagedSqlList<AdminMenuModel> SearchAdminMenu(AdminMenuCriteria criteria)
        {
            var queryList = new List<AdminMenuModel>();
            var totalCount = 0;
            var ds = ProductDataAccess.SearchAdminMenuDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new AdminMenuModel()
                        {
                            CreateTime = dr["CreateTime"].ToString(),
                            MenuName = dr["MenuName"].ToString(),
                            Sorft = Convert.ToInt32(dr["Sorft"]),
                            LoginName = dr["LoginName"].ToString(),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            Id = Convert.ToInt32(dr["Id"]),
                            ShopAdminId = Convert.ToInt32(dr["ShopAdminId"])
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<AdminMenuModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public List<ProductBannerModel> GetProductBannerList(int shopAdminId)
        {
            var queryList = new List<ProductBannerModel>();
            var ds = ProductDataAccess.GetProductBannerList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ProductBannerModel()
                        {
                            ProductId = SecretClass.EncryptQueryString(dr["ProductId"].ToString()),
                            Id = Convert.ToInt32(dr["Id"]),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            Sorft = Convert.ToInt32(dr["Sorft"]),
                            ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString()
                        });
                    }
                }

            }
            return queryList;
        }
        public ProductBannerModel GetProductBanner(int id)
        {
            var res = new ProductBannerModel();
            var ds = ProductDataAccess.GetProductBanner(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString();
                    res.Sorft = Convert.ToInt32(dr["Sorft"]);
                    res.ProductId = SecretClass.EncryptQueryString(dr["ProductId"].ToString());
                }
            }
            return res;
        }
        public int AddMenu(MenuModel model, out string msg)
        {
            //给路径赋值，如果包含图片服务器URL则为旧图，否则为新图，月文件夹最新。
            if (model.ImgPath.Contains(ConfigSettings.Instance.ImageUrl))
                model.ImgPath = model.ImgPath.Replace(ConfigSettings.Instance.ImageUrl, "");
            else
                model.ImgPath = ProductMenuUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(model.ImgPath);
            var ret = ProductDataAccess.AddMenu(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "已存在同名分类";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public bool AddProduct(ProductModel model)
        {
            var imgList = new List<ProductImgModel>();
            int _count = 1;
            model.BannerImgs.ForEach((img) =>
            {
                var imgModel = new ProductImgModel();
                if (_count == 1)
                    imgModel.IsDefault = 1;
                imgModel.ImagePath = ProductBannerUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(img);
                imgModel.ImageType = 1;//压缩图
                imgList.Add(imgModel);
                _count++;
            });
            return ProductDataAccess.AddProduct(model, imgList);
        }

        public bool AddProductBanner(ProductBannerModel model)
        {
            //给路径赋值，如果包含图片服务器URL则为旧图，否则为新图，月文件夹最新。
            if (model.ImgPath.Contains(ConfigSettings.Instance.ImageUrl))
                model.ImgPath = model.ImgPath.Replace(ConfigSettings.Instance.ImageUrl, "");
            else
                model.ImgPath = ProductBannerUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(model.ImgPath);
            return ProductDataAccess.AddProductBanner(model);
        }
        public bool EditProductBanner(ProductBannerModel model)
        {
            //给路径赋值，如果包含图片服务器URL则为旧图，否则为新图，月文件夹最新。
            if (model.ImgPath.Contains(ConfigSettings.Instance.ImageUrl))
                model.ImgPath = model.ImgPath.Replace(ConfigSettings.Instance.ImageUrl, "");
            else
                model.ImgPath = ProductBannerUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(model.ImgPath);
            return ProductDataAccess.EditProductBanner(model);
        }

        public void OptionProductBanner(int id, int optionStatus)
        {
            ProductDataAccess.OptionProductBanner(id, optionStatus);
        }
        public void DelProductBanner(int id)
        {
            ProductDataAccess.DelProductBanner(id);
        }

        public bool EditProduct(ProductModel model)
        {
            var imgList = new List<ProductImgModel>();
            int _count = 1;
            model.BannerImgs.ForEach((img) =>
            {
                var imgModel = new ProductImgModel();
                if (_count == 1)
                    imgModel.IsDefault = 1;
                //给路径赋值，如果包含图片服务器URL则为旧图，否则为新图，月文件夹最新。
                if (img.Contains(ConfigSettings.Instance.ImageUrl))
                    imgModel.ImagePath = img.Replace(ConfigSettings.Instance.ImageUrl, "");
                else
                    imgModel.ImagePath = ProductBannerUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(img);
                imgModel.ImageType = 1;//压缩图
                imgList.Add(imgModel);
                _count++;
            });
            return ProductDataAccess.EditProduct(model, imgList);
        }

        public int DeleteMenu(int id, int shopAdminId, out string msg)
        {
            var ret = ProductDataAccess.DeleteMenu(id, shopAdminId);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "当前分类下有未删除的商品";
                    break;
                case 3:
                    msg = "分类不存在";
                    break;
                case 4:
                    msg = "当前分类下有正在使用的专场";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }


        public int EditMenu(MenuModel model, out string msg)
        {
            //给路径赋值，如果包含图片服务器URL则为旧图，否则为新图，月文件夹最新。
            if (model.ImgPath.Contains(ConfigSettings.Instance.ImageUrl))
                model.ImgPath = model.ImgPath.Replace(ConfigSettings.Instance.ImageUrl, "");
            else
                model.ImgPath = ProductMenuUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(model.ImgPath);
            var ret = ProductDataAccess.EditMenu(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "已存在同名分类";
                    break;
                case 3:
                    msg = "分类不存在";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public List<MenuModel> MenuList(int shopAdminId)
        {
            var res = new List<MenuModel>();
            var ds = ProductDataAccess.MenuList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new MenuModel()
                    {
                        Id = Convert.ToInt32(dr["Id"].ToString()),
                        MenuName = dr["MenuName"].ToString(),
                        Sorft = Convert.ToInt32(dr["Sorft"]),
                        IsAll = Convert.ToInt32(dr["IsAll"]),
                        OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                        ImgPath = dr["ImgPath"] == DBNull.Value ? "" : ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                        CreateTime = dr["CreateTime"].ToString()
                    });
                }
            }
            return res;
        }

        public int OptionMenu(int id, int optionStatus, int shopAdminId, out string msg)
        {
            var ret = ProductDataAccess.OptionMenu(id, optionStatus, shopAdminId);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "当前分类下有正在上架的商品";
                    break;
                case 3:
                    msg = "分类不存在";
                    break;
                case 4:
                    msg = "当前分类下有正在上架的专场";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public int OptionProduct(int id, int optionStatus, int shopAdminId, out string msg)
        {
            var ret = ProductDataAccess.OptionProduct(id, optionStatus, shopAdminId);
            msg = string.Empty;
            switch (ret)
            {
                case 3:
                    msg = "商品不存在";
                    break;
                case 4:
                    msg = "请先设置商品SKU";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }
        public int DeleteProduct(int id, int shopAdminId, out string msg)
        {
            var ret = ProductDataAccess.DeleteProduct(id, shopAdminId);
            msg = string.Empty;
            switch (ret)
            {
                case 3:
                    msg = "商品不存在";
                    break;
                case 4:
                    msg = "请先下架商品";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public int AddPropety(AddPropetyParamsModel model, out string msg)
        {
            var ret = ProductDataAccess.AddPropety(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "规格最多三种";
                    break;
                case 3:
                    msg = "已存在规格名称";
                    break;
                case 4:
                    msg = "事务出错";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public ProductModel GetProduct(int id)
        {
            var res = new ProductModel();
            var ds = ProductDataAccess.GetProduct(id);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]) && DataManager.CheckHasRow(ds.Tables[1]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.ProductName = dr["ProductName"].ToString();
                    res.Brand = dr["Brand"].ToString();
                    res.Title = dr["Title"].ToString();
                    res.MenuId = Convert.ToInt32(dr["MenuId"]);
                    res.SallCount = DataManager.GetRowValue_Int(dr["SallCount"]);
                    res.ShipTemplateId = DataManager.GetRowValue_Int(dr["ShippingTemplateId"]);
                    res.IsHot = DataManager.GetRowValue_Int(dr["IsHot"]);
                    res.Unit = dr["Unit"].ToString();
                    res.Sorft = Convert.ToInt32(dr["Sorft"]);
                    res.InitSallNum = Convert.ToInt32(dr["InitSallNum"]);
                    res.ShopAdminId = Convert.ToInt32(dr["ShopAdminId"]);
                    res.Detail = dr["ProductDetail"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.BannerImgs = new List<string>();
                    foreach (DataRow dr_img in ds.Tables[1].Rows)
                    {
                        res.BannerImgs.Add(ConfigSettings.Instance.ImageUrl + dr_img["ImagePath"].ToString());
                    }
                }
            }
            return res;
        }

        public ProductSkuManagerModel GetProductSkuManagerModel(int shopAdminId, string productId)
        {
            var res = new ProductSkuManagerModel();
            res.ProductId = SecretClass.EncryptQueryString(productId);
            var ds = ProductDataAccess.GetProductSkuDs(shopAdminId, productId);
            if (DataManager.CheckDs(ds, 2))
            {
                //表1：获取当前商品单规格SKU数据
                //表2：获取当前商品的规格类型

                res.SkuType = Convert.ToInt32(ds.Tables[1].Rows[0]["CurrentSkuType"]);
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.SingleSkuModel.Id = Convert.ToInt32(dr["Id"]);
                    res.SingleSkuModel.ProductId = res.ProductId;
                    res.SingleSkuModel.SallPrice = Convert.ToDecimal(dr["SallPrice"]);
                    res.SingleSkuModel.SkuCode = dr["SkuCode"].ToString();
                    res.SingleSkuModel.SkuType = 1;
                    res.SingleSkuModel.Weight = DataManager.GetRowValue_Float(dr["Weight"]);
                    res.SingleSkuModel.StockNum = Convert.ToInt32(dr["StockNum"]);
                }

            }
            return res;
        }

        public ProductSkuManagerModel GetProductSkuManagerModel_AJAX(int shopAdminId, string productId)
        {
            var res = new ProductSkuManagerModel();
            res.ProductId = SecretClass.EncryptQueryString(productId);
            var ds = ProductDataAccess.GetProductMoreSkuDs(shopAdminId, productId);
            if (DataManager.CheckDs(ds, 3))
            {
                //表1：获取当前商品多规格SKU数据
                //表2：获取当前商品多规格SKU属性数据
                //表3：获取当前商品多规格SKU属性值数据
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.MoreSkuModelList.Add(new MoreSkuModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            ProductId = res.ProductId,
                            PropetyCombineId = dr["PropetyCombineId"].ToString(),
                            ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                            StockNum = Convert.ToInt32(dr["StockNum"]),
                            SallPrice = Convert.ToDecimal(dr["SallPrice"]),
                            Weight = DataManager.GetRowValue_Float(dr["Weight"]),
                            SkuType = 2,
                            SkuCode = dr["SkuCode"].ToString()
                        });
                    }
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.Product_Sall_PropetyModelList.Add(new Product_Sall_PropetyModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            PropetyName = dr["PropetyName"].ToString(),
                            ProductId = res.ProductId
                        });
                    }

                }

                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        res.Product_Sall_Propety_ValueModelList.Add(new Product_Sall_Propety_ValueModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            SallPropetyId = Convert.ToInt32(dr["SallPropetyId"]),
                            PropetyValue = dr["PropetyValue"].ToString(),
                            ProductId = res.ProductId
                        });
                    }

                }
                //将属性值模型，分别装入属性名称属性中
                if (res.Product_Sall_PropetyModelList.Count > 0)
                {
                    foreach (var propety in res.Product_Sall_PropetyModelList)
                    {
                        var _propetyValueList = res.Product_Sall_Propety_ValueModelList.Where(p => p.SallPropetyId == propety.Id).ToList();
                        propety.Product_Sall_Propety_ValueList.AddRange(_propetyValueList);
                    }
                }
            }
            return res;
        }

        public bool EditSingleSku(SingleSkuModel model)
        {
            return ProductDataAccess.EditSingleSku(model);
        }
        public bool EditMoreSku(ProductSkuManagerModel model, int shopAdminId)
        {
            foreach (var sku in model.MoreSkuModelList)
            {
                sku.SkuCode = sku.SkuCode == null ? "" : sku.SkuCode;
                //由于不能参数化，要手工检测SQL注入
                if (ToolManager.IsSqlin(sku.PropetyCombineId) || ToolManager.IsSqlin(sku.ProppetyCombineName) || ToolManager.IsSqlin(sku.SkuCode))
                {
                    LogManger.Instance.WriteLog("商品ID为" + model.ProductId + "，店主后台ID为" + shopAdminId + "，发生了SQL注入,PropetyCombineId:" + sku.PropetyCombineId + ",ProppetyCombineName:" + sku.ProppetyCombineName + ",SkuCode:" + sku.SkuCode);
                    return false;
                }
            }
            return ProductDataAccess.EditMoreSku(model, shopAdminId);
        }

        public int AddPropetyValue(Product_Sall_Propety_ValueModel model, out string msg)
        {
            var ret = ProductDataAccess.AddPropetyValue(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "当前规格已存在该规格值";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public bool DelPropetyValue(Product_Sall_Propety_ValueModel model)
        {
            return ProductDataAccess.DelPropetyValue(model);
        }
        public bool DelPropety(int id, string productId, int shopAdminId)
        {
            return ProductDataAccess.DelPropety(id, productId, shopAdminId);
        }
        public List<SpecialPlaceModel> SpecialPlaceList(int shopAdminId)
        {
            var res = new List<SpecialPlaceModel>();
            var ds = ProductDataAccess.SpecialPlaceList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new SpecialPlaceModel()
                    {
                        Id = Convert.ToInt32(dr["Id"].ToString()),
                        PlaceName = dr["PlaceName"].ToString(),
                        Sorft = Convert.ToInt32(dr["Sorft"]),
                        OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                        SpecialType = DataManager.GetRowValue_Int(dr["SpecialType"]),
                        ImgPath = dr["ImgPath"] == DBNull.Value ? "" : ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                        MenuName = dr["MenuName"].ToString(),
                        MenuId = Convert.ToInt32(dr["MenuId"])

                    });
                }
            }
            return res;
        }

        public void DelSpecialPlace(int id)
        {
            ProductDataAccess.DelSpecialPlace(id);
        }
        public bool InsertOrEditSpecialPlace(SpecialPlaceModel model)
        {
            //给路径赋值，如果包含图片服务器URL则为旧图，否则为新图，月文件夹最新。
            if (model.ImgPath.Contains(ConfigSettings.Instance.ImageUrl))
                model.ImgPath = model.ImgPath.Replace(ConfigSettings.Instance.ImageUrl, "");
            else
                model.ImgPath = ProductMenuUploader.Instance.FolderName + "/" + DateTime.Now.ToString("yyyyMMdd") + "/" + Path.GetFileName(model.ImgPath);
            return ProductDataAccess.InsertOrEditSpecialPlace(model);
        }
        public SpecialPlaceModel GetSpecialPlaceModel(int id)
        {
            var res = new SpecialPlaceModel();
            var ds = ProductDataAccess.GetSpecialPlaceDs(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.MenuId = Convert.ToInt32(dr["MenuId"]);
                    res.PlaceName = dr["PlaceName"].ToString();
                    res.SpecialType = DataManager.GetRowValue_Int(dr["SpecialType"]);
                    res.Sorft = Convert.ToInt32(dr["Sorft"]);
                    res.ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString();
                }
            }
            return res;
        }
        #region 接口

        public List<SpecialPlaceModel> SpecialPlaceList(int shopAdminId, int type = 1)
        {
            var res = new List<SpecialPlaceModel>();
            var ds = ProductDataAccess.SpecialPlaceList(shopAdminId, type);
            if (DataManager.CheckDs(ds, 1))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new SpecialPlaceModel()
                    {
                        Id = Convert.ToInt32(dr["Id"].ToString()),
                        PlaceName = dr["PlaceName"].ToString(),
                        Sorft = Convert.ToInt32(dr["Sorft"]),
                        OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                        SpecialType = DataManager.GetRowValue_Int(dr["SpecialType"]),
                        ImgPath = dr["ImgPath"] == DBNull.Value ? "" : ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                        MenuName = dr["MenuName"].ToString(),
                        MenuId = Convert.ToInt32(dr["MenuId"])

                    });
                }
            }
            return res;
        }
        public List<Api_ProductItem> Api_GetBanners(int said)
        {
            var res = new List<Api_ProductItem>();
            var ds = ProductDataAccess.Api_GetBanners(said);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new Api_ProductItem()
                    {
                        Id = SecretClass.EncryptQueryString(dr["ProductId"].ToString()),
                        ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                        ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString()

                    });
                }
            }
            return res;
        }
        public List<Api_ProductItem> Api_GetHotProducts(int said)
        {
            var res = new List<Api_ProductItem>();
            var ds = ProductDataAccess.Api_GetHotProducts(said);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new Api_ProductItem()
                    {
                        Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                        ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                        SallNum = DataManager.GetRowValue_Int(dr["SallNum"]),
                        ProductName = dr["ProductName"].ToString(),
                        Brand = dr["Brand"].ToString(),
                        Title = dr["Title"].ToString(),
                        ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                        SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"])

                    });
                }
            }
            return res;
        }
        public List<Api_ProductItem> Api_GetCommandProducts(int said)
        {
            var res = new List<Api_ProductItem>();
            var ds = ProductDataAccess.Api_GetCommandProducts(said);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new Api_ProductItem()
                    {
                        Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                        ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                        ProductName = dr["ProductName"].ToString(),
                        Brand = dr["Brand"].ToString(),
                        Title = dr["Title"].ToString(),
                        ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                        SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"])

                    });
                }
            }
            return res;
        }
        public Api_HomeDataModel Api_GetHomeData(int shopAppId)
        {
            var res = new Api_HomeDataModel();
            var ds = ProductDataAccess.Api_GetHomeData(shopAppId);
            if (DataManager.CheckDs(ds, 3))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Banners.Add(new Api_ProductItem()
                        {
                            Id = SecretClass.EncryptQueryString(dr["ProductId"].ToString()),
                            ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                            ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString()
                        });
                    }
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.NewRecommends.Add(new Api_ProductItem()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                            ProductName = dr["ProductName"].ToString(),
                            Brand = dr["Brand"].ToString(),
                            Title = dr["Title"].ToString(),
                            ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                            SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"])
                        });
                    }
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        res.Hots.Add(new Api_ProductItem()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                            ProductName = dr["ProductName"].ToString(),
                            Brand = dr["Brand"].ToString(),
                            Title = dr["Title"].ToString(),
                            ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                            SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"])
                        });
                    }
                }
            }
            return res;
        }

        public List<Api_MenuItemModel> Api_GetMenuList(int shopAdminId)
        {
            var res = new List<Api_MenuItemModel>();
            var ds = ProductDataAccess.Api_GetMenuList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new Api_MenuItemModel()
                    {
                        Id = Convert.ToInt32(dr["Id"].ToString()),
                        MenuName = dr["MenuName"].ToString(),
                        IsAll = DataManager.GetRowValue_Int(dr["IsAll"]),
                        ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString()
                    });
                }
            }
            return res;
        }

        public List<Api_ProductItem> Api_GetProductList(Api_ProductListCriteria criteria)
        {
            var res = new List<Api_ProductItem>();
            var ds = ProductDataAccess.Api_GetProductList(criteria);
            if (DataManager.CheckDs(ds, 1))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new Api_ProductItem()
                    {
                        Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                        ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                        ProductName = dr["ProductName"].ToString(),
                        Brand = dr["Brand"].ToString(),
                        Title = dr["Title"].ToString(),
                        ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString()),
                        SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"])
                    });
                }
            }
            return res;
        }

        public Api_ProductDetailModel Api_GetProduct(int id)
        {
            var res = new Api_ProductDetailModel();
            var ds = ProductDataAccess.Api_GetProduct(id);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]) && DataManager.CheckHasRow(ds.Tables[1]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.ProductName = dr["ProductName"].ToString();
                    res.Brand = dr["Brand"].ToString();
                    res.Title = dr["Title"].ToString();
                    res.MenuId = Convert.ToInt32(dr["MenuId"]);
                    res.ShipTemplateId = DataManager.GetRowValue_Int(dr["ShippingTemplateId"]);
                    res.Unit = dr["Unit"].ToString();
                    res.CurrentSkuType = Convert.ToInt32(dr["CurrentSkuType"]);
                    res.Detail = dr["ProductDetail"].ToString();
                    res.MinAmount = DataManager.GetRowValue_Decimal(dr["MinAmount"]);
                    res.MaxAmount = DataManager.GetRowValue_Decimal(dr["MaxAmount"]);
                    res.BannerImgs = new List<string>();
                    foreach (DataRow dr_img in ds.Tables[1].Rows)
                    {
                        res.BannerImgs.Add(ConfigSettings.Instance.ImageUrl + dr_img["ImagePath"].ToString());
                    }
                }
            }
            return res;
        }
        public List<Api_CreaterOrder_CheckItem> Api_GetCreateOrder_CheckItemList(string skuids)
        {
            //验证
            if (ToolManager.IsSqlin(skuids))
            {
                LogManger.Instance.WriteLog("创建订单发生SQL注入:注入字符串" + skuids);
                return null;
            }
            var queryList = new List<Api_CreaterOrder_CheckItem>();
            var ds = ProductDataAccess.Api_GetCreateOrder_CheckItemDs(skuids);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new Api_CreaterOrder_CheckItem()
                        {
                            ProductSkuId = Convert.ToInt32(dr["ProductSkuId"]),
                            StockNum = Convert.ToInt32(dr["StockNum"]),
                            IsDel = Convert.ToInt32(dr["IsDel"]),
                            Product_OptionStatus = Convert.ToInt32(dr["Product_OptionStatus"]),
                            Product_IsDel = Convert.ToInt32(dr["Product_IsDel"]),
                            ProductName = dr["ProductName"].ToString(),
                            ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                            SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"]),
                            SkuCode = dr["SkuCode"].ToString(),
                            PropetyCombineId = dr["PropetyCombineId"].ToString(),
                            Weight = DataManager.GetRowValue_Float(dr["Weight"])
                        });
                    }
                }

            }
            return queryList;
        }


        #endregion

        public void Dispose()
        {

        }

    }
}
