﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface ITestService :IService
    {
        List<TestModel> TestList();
    }
}
