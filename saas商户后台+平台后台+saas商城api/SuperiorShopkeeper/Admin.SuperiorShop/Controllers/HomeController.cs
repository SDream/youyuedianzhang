﻿using System.Web.Mvc;
using SuperiorShopBussinessService;

namespace Admin.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException]
    public class HomeController : BaseController
    {
        private readonly IHomeService _IHomeService;
        private readonly IShopService _IShopService;

        public HomeController(IHomeService IHomeService, IShopService IShopService)
        {
            _IHomeService = IHomeService;
            _IShopService = IShopService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IHomeService.Dispose();
            this._IShopService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _Index()
        {
            var model = _IHomeService.GetAdminHomeData();
            return View(model);
        }

        /// <summary>
        /// 获取近7日每日用户增长量报表数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetAdminAddUserChart()
        {
            var model = _IHomeService.GetAdminAddUserChart();
            return Success(model);
        }

        /// <summary>
        /// 获用户类型分布的报表数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetAdminTypeUserChart()
        {
            var model = _IHomeService.GetAdminTypeUserChart();
            return Success(model);
        }
        /// <summary>
        /// 获取近7日每日销售额和销售量报表数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetAdminSallChart()
        {
            var model = _IHomeService.GetAdminSallChart();
            return Success(model);
        }
    }
}