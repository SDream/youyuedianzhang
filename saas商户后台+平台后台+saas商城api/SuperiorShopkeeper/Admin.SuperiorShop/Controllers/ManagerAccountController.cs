﻿using SuperiorShopBussinessService;
using System.Web.Mvc;
using SuperiorModel;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class ManagerAccountController : BaseController
    {
        private readonly IManagerAccountService _IManagerAccountService;

        public ManagerAccountController(IManagerAccountService IManagerAccountService)
        {
            _IManagerAccountService = IManagerAccountService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IManagerAccountService.Dispose();
            base.Dispose(disposin);
        }
        /// <summary>
        /// 推广人列表
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagerAccountList()
        {
            var criteria = new ManagerAccountListCriteria() { OptionStatus = 999, PagingResult = new PagingResult(0, 20) };
            var model = GetTpagedManagerAccountModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 推广人列表搜索
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        public ActionResult ManagerAccountList(ManagerAccountListCriteria criteria)
        {
            var model = GetTpagedManagerAccountModel(criteria);
            return PartialView("_ManagerAccountList", model);
        }


        private TPagedModelList<ManagerAccountModel> GetTpagedManagerAccountModel(ManagerAccountListCriteria criteria)
        {
            var query = _IManagerAccountService.SearchManagerAccountList(criteria);
            return new TPagedModelList<ManagerAccountModel>(query, query.PagingResult);
        }

        // GET: ManagerAccount
        /// <summary>
        /// 推广人提现订单列表
        /// </summary>
        /// <returns></returns>
        public ActionResult ManagerAccountOrderList()
        {
            var criteria = new ManagerAccountOrderListCriteria() { OrderStatus = 999, PagingResult = new PagingResult(0, 20) };
            var model = GetTpagedManagerAccountOrderModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 推广人订单列表搜索
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        public ActionResult ManagerAccountOrderList(ManagerAccountOrderListCriteria criteria)
        {
            var model = GetTpagedManagerAccountOrderModel(criteria);
            return PartialView("_ManagerAccountOrderList", model);
        }

        private TPagedModelList<ManagerAccountOrderModel> GetTpagedManagerAccountOrderModel(ManagerAccountOrderListCriteria criteria)
        {
            var query = _IManagerAccountService.SearchManagerAccountOrderList(criteria);
            return new TPagedModelList<ManagerAccountOrderModel>(query, query.PagingResult);
        }
        /// <summary>
        /// 操作推广人状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult OptionManagerAccount(int id, int option)
        {
            _IManagerAccountService.OptionManagerAccount(id, option);
            return Success(true);
        }
        /// <summary>
        /// 确认给推广人转账，实际只是更改下状态，有企业微信转账后，这主要做转账失败，后操作。
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult TransferAmount(string orderno)
        {
            _IManagerAccountService.UpdateCashCoutOder(orderno, 1);
            return Success(true);
        }
    }
}