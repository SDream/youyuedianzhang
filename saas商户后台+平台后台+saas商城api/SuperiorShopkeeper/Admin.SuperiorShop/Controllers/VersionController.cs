﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Redis;
using SuperiorModel;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class VersionController : BaseController
    {
        // GET: Version
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 版本管理
        /// </summary>
        /// <returns></returns>
        public ActionResult List()
        {
            var list = RedisManager.Get<List<VersionItem>>("Version");
            if (list == null)
            {
                list = new List<VersionItem>();
                RedisManager.Set<List<VersionItem>>("Version",list,DateTime.Now.AddYears(20));
            }
            return View(list.OrderByDescending(p=>p.CreateTime).ToList());
        }
        /// <summary>
        /// 添加薪版本
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public JsonResult Add(VersionItem model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.CreateTime = DateTime.Now;
            RedisManager.AddItemToListT<VersionItem>("Version", model);
            return Success(true);
        }
    }
}