﻿using SuperiorShopBussinessService;
using System.Web.Mvc;
using SuperiorModel;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class DataController : BaseController
    {
        private readonly IDataService _IDataService;
      

        public DataController(IDataService IDataService)
        {
            _IDataService = IDataService;
           
        }
        protected override void Dispose(bool disposin)
        {
            this._IDataService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Data
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 商户订单排行
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopAdminOrderStatistics()
        {
            var criteria = new ShopOrderRankCriteria() { PagingResult = new PagingResult(0, 20) };
            var model = GetShopOrderPagedList(criteria);
            return View(model);
        }
        private TPagedModelList<ShopOrderRankModel> GetShopOrderPagedList(ShopOrderRankCriteria criteria)
        {
            var query = _IDataService.SearchShopOrderRankDs(criteria);
            return new TPagedModelList<ShopOrderRankModel>(query, query.PagingResult);
        }
        /// <summary>
        /// 搜索商户订单排行
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult ShopAdminOrderStatistics(ShopOrderRankCriteria criteria)
        {
            var model = GetShopOrderPagedList(criteria);
            return PartialView("_ShopAdminOrderStatistics", model);
        }

        /// <summary>
        /// 商户用户量排行
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopAdminUserStatistics()
        {
            var criteria = new ShopUserRankCriteria() { PagingResult = new PagingResult(0, 20) };
            var model = GetShopUserPagedList(criteria);
            return View(model);
        }
        private TPagedModelList<ShopUserRankModel> GetShopUserPagedList(ShopUserRankCriteria criteria)
        {
            var query = _IDataService.SearchShopUserRankDs(criteria);
            return new TPagedModelList<ShopUserRankModel>(query, query.PagingResult);
        }
        /// <summary>
        /// 搜索商户用户排行
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult ShopAdminUserStatistics(ShopUserRankCriteria criteria)
        {
            var model = GetShopUserPagedList(criteria);
            return PartialView("_ShopAdminUserStatistics", model);
        }

        /// <summary>
        /// 商城到期时间排行
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopAppExpressStatistics()
        {
            var criteria = new ShopAppExpressRankCriteria() { PagingResult = new PagingResult(0, 20),AppType=999 };
            var model = GetShopAppExpressPagedList(criteria);
            return View(model);
        }
        private TPagedModelList<ShopAppExpressRankModel> GetShopAppExpressPagedList(ShopAppExpressRankCriteria criteria)
        {
            var query = _IDataService.SearchShopAppExpressRankDs(criteria);
            return new TPagedModelList<ShopAppExpressRankModel>(query, query.PagingResult);
        }
        /// <summary>
        /// 搜索商城到期时间排行
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult ShopAppExpressStatistics(ShopAppExpressRankCriteria criteria)
        {
            var model = GetShopAppExpressPagedList(criteria);
            return PartialView("_ShopAppExpressStatistics", model);
        }

    }
}