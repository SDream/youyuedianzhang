﻿using SuperiorCommon;
using System;
using System.Web;
using System.Web.Mvc;

namespace Admin.SuperiorShop
{
    /// <summary>
    /// 是否进行跳转登录过滤器
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckLoginFitler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session[SessionKey.AdminnKey] == null)
            {
                filterContext.Result = new RedirectResult("/Login/Login?notice=timeout");
                return;
            }

        }
    }
}