﻿(function () {
    UserListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 30 }, AppType: 999, OptionStatus :999};
    UserListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;
                laydate.render({
                    elem: '#search_BeginTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
                laydate.render({
                    elem: '#search_EndTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
            });
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, UserListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },
        Search: function () {

            _searchCriteria.NickName = $("#search_NickName").val();
            _searchCriteria.AppType = $("#search_AppType").val();
            _searchCriteria.LoginName = $.trim($("#search_LoginName").val());
            _searchCriteria.OptionStatus = $("#search_status").val();
            _searchCriteria.BeginTime = $("#search_BeginTime").val();
            _searchCriteria.EndTime = $("#search_EndTime").val();
            UserListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/User/List", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()