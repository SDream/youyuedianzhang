﻿(function () {
    CouponListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }, CouponType:999 };
    CouponListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });

            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, CouponListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },
        Search: function () {
            _searchCriteria.LoginName = $("#search_LoginName").val();
            _searchCriteria.CouponType = $.trim($("#search_CouponType").val());
            CouponListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Marketing/CouponList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()