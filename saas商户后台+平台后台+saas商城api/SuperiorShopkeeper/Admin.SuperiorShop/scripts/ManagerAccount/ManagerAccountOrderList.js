﻿(function () {
    ManagerAccountOrderListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }, OrderStatus: 999 };
    ManagerAccountOrderListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });

            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ManagerAccountOrderListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.transferAmount', this.Transfer);
        },
        Transfer: function () {
            var orderno = $(this).attr("OrderNo");


            layer.confirm('请确认已经微信转完账了,即将更改订单状态', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/ManagerAccount/TransferAmount?orderno=" + orderno + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.msg("执行成功");
                        ManagerAccountOrderListClass.Instance.Refresh();


                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });



        },
        Search: function () {
            _searchCriteria.OrderNo = $("#search_OrderNo").val();
            _searchCriteria.PhoneNumber = $("#search_PhoneNumber").val();
            _searchCriteria.TrueName = $("#search_TrueName").val();
            _searchCriteria.OrderStatus = $("#search_OrderStatus").val();
            ManagerAccountOrderListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ManagerAccount/ManagerAccountOrderList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()