﻿(function () {
    ManagerAccountListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }, OptionStatus :999};
    ManagerAccountListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
         
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ManagerAccountListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.OptionAccount', this.OptionStatus);
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/ManagerAccount/OptionManagerAccount?id=" + id + "&option=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/ManagerAccount/ManagerAccountList";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Search: function () {

            _searchCriteria.PhoneNumber = $("#search_PhoneNumber").val();
            _searchCriteria.TrueName = $("#search_TrueName").val();
            _searchCriteria.OptionStatus = $("#search_OptionStatus").val();
            ManagerAccountListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ManagerAccount/ManagerAccountList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()