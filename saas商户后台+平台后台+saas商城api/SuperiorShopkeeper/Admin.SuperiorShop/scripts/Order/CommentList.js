﻿(function () {
    CommentListClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 } };
    CommentListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();

            });

            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, CommentListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.delComment', this.Del);
            $(document).on('click', '.showMsg', this.ShowMsg);
        },
        ShowMsg: function () {
            var msg = $(this).attr("Msg");
            layer.alert(msg);
        },
        Del:function(){
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/Order/DelComment?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/Order/CommentList";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
        Search: function () {

            _searchCriteria.OrderNo = $("#search_OrderNo").val();
            _searchCriteria.CommentLevel = $("#search_level").val();
            _searchCriteria.LoginName = $("#search_loginName").val();
            CommentListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Order/CommentList";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post(url, _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()