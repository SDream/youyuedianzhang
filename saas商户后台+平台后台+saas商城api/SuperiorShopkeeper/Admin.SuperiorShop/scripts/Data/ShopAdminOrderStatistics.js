﻿(function () {
    ShopAdminOrderStatisticsClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 } };
    ShopAdminOrderStatisticsClass.Instance = {
        Init: function () {
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ShopAdminOrderStatisticsClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },

        Search: function () {
            _searchCriteria.LoginName = $("#search_LoginName").val();
            ShopAdminOrderStatisticsClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Data/ShopAdminOrderStatistics";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Data/ShopAdminOrderStatistics", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }

    };

})();