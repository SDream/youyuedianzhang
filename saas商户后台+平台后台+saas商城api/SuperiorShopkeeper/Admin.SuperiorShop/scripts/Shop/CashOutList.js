﻿(function () {
    CashOutListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 15 }, OrderStatus: 999 };
    CashOutListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });

            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, CashOutListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.OptionOrder', this.OptionOrder);
        },
        OptionOrder: function () {
            var orderno = $(this).attr("OrderNo");
            layer.confirm('请确认已经人工处理完毕,此操作是将订单改为成功!', {
                btn: ['确定执行', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/Shop/OptionOrder?orderno=" + orderno + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.msg("执行成功");
                        CashOutListClass.Instance.Refresh();

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {
                
            });
        },
        Search: function () {

            _searchCriteria.OrderStatus = $("#search_status").val();
            _searchCriteria.LoginName = $("#search_LoginName").val();
            CashOutListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Shop/CashOutList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()