﻿using System.Configuration;

namespace SuperiorSqlTools
{
    public class DALConfig
    {
        public static string GetConnStr(string connKey)
        {
            return ConfigurationManager.ConnectionStrings[connKey].ConnectionString;
        }

        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["SuperiorShop"].ConnectionString; }//form System.Configuration.dll
        }

        public static string ErpConnectionString
        {
            get { return ConfigurationManager.AppSettings["ErpConnectonStrings"]; }
        }
    }
}
