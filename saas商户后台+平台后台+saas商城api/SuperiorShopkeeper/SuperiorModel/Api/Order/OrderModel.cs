﻿using System.Collections.Generic;

namespace SuperiorModel
{
    public class Api_CreateOrderCriteria
    {
        public string Spid { get; set; }
        public string Said { get; set; }
        public string UserId { get; set; }
        public int ConsigneeId { get; set; }
        public string Remarks { get; set; }
        public int CouponId { get; set; }
        public decimal ShipAmount { get; set; }
        public decimal ToalAmount { get; set; }
        public List<Api_CreateOrder_ProductItem> Api_CreateOrder_ProductItems { get; set; }
        public int AppType { get; set; }
        public decimal  ProductAmount { get; set; }
        public decimal CouponAmount { get; set; }

    }
    public class Api_CreateOrder_ProductItem
    {
        public string ProductId { get; set; }
        public int Product_SkuId { get; set; }
        public int SaleNum { get; set; }
        public Api_CreaterOrder_CheckItem Api_CreaterOrder_CheckItem { get; set; }
    }
    public class Api_CreaterOrder_CheckItem
    {
        public int ProductSkuId { get; set; }
        public int StockNum { get; set; }
        public int IsDel { get; set; }
        public int Product_OptionStatus { get; set; }
        public int Product_IsDel { get; set; }
        public string ProductName { get; set; }
        public string ProppetyCombineName { get; set; }
        public decimal SallPrice { get; set; }
        public string SkuCode { get; set; }
        public string PropetyCombineId { get; set; }
        public float Weight { get; set; }

    }
    
    public class Api_WeixinCreateOrderModel
    {
        public string OrderNumber { get; set; }
        /// <summary>
        /// 支付key
        /// </summary>
        public string Paykey { get; set; }
        /// <summary>
        /// 应用APPID
        /// </summary>
        public string Appid { get; set; }
        /// <summary>
        /// 支付商户号
        /// </summary>
        public string Mch_id { get; set; }
        /// <summary>
        /// 用户openid
        /// </summary>
        public string Openid { get; set; }
        public double TotalFee { get; set; }

        public string ProductBody { get; set; }
        /// <summary>
        /// 1.我司代收，2自有支付，
        /// </summary>
        public int PayType { get; set; }
        public int OrderID { get; set; }

        public int AppType { get; set; }

    }
    
    public class Api_CreateOrderResultModel
    {
        public Api_OrderPayResultModel PayParamater { get; set; }
        /// <summary>
        /// int ,0：积分，1：微信支付,2:银行分期付款
        /// </summary>
        public int PayType { get; set; }

        public long OrderID { get; set; }
    }
    
    public class Api_OrderPayResultModel
    {
        public string AppId { get; set; }
        public string TimeStamp { get; set; }
        public string NonceStr { get; set; }
        public string Package { get; set; }
        public string SignType { get; set; }
        public string PaySign { get; set; }

        public string Payorderno { get; set; }

        public string Orderno { get; set; }
        public long OrderID { get; set; }
    }
    
    public class Api_OrderDetail_InfoModel
    {
        public Api_OrderDetail_InfoModel()
        {
            this.Products = new List<Api_OrderDetail_ProductItem>();
        }
        public int OrderID { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneePhone { get; set; }
        public string Address { get; set; }
        public decimal RealityAmount { get; set; }
        public string OrderNo { get; set; }
        public string Remark { get; set; }
        public int OrderStatus { get; set; }
        public string CreateTime { get; set; }
        public string PayTime { get; set; }
        public decimal CouponDelAmount { get; set; }
        public decimal ShippingAmount { get; set; }
        public decimal OrderAmount { get; set; }
        public string CouponName { get; set; }
        /// <summary>
        /// 快递名称
        /// </summary>
        public string DeliveryName { get; set; }
        /// <summary>
        /// 快递单号
        /// </summary>
        public string DeliveryNo { get; set; }
        public List<Api_OrderDetail_ProductItem> Products { get; set; }
        /// <summary>
        /// 订单评论ID
        /// </summary>
        public int CommentId { get; set; }

    }
    
    public class Api_OrderDetail_ProductItem
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProppetyCombineName { get; set; }
        public int BuyNum { get; set; }
        public decimal SalePrice { get; set; }
        public string ImagePath { get; set; }
    }
    
    public class Api_OrderListModel
    {
        public int OrderID { get; set; }
        public string OrderNo { get; set; }
        public int ProductNum { get; set; }
        public decimal RealityAmount { get; set; }
        public int OrderStatus { get; set; }
        /// <summary>
        /// 订单详情中第一个商品得ID
        /// </summary>
        public string ProductId { get; set; }
        /// <summary>
        /// 订单详情中第一个商品名称，以下类推
        /// </summary>
        public string ProductName { get; set; }
        public string ProppetyCombineName { get; set; }
        public int BuyNum { get; set; }
        public decimal SalePrice { get; set; }
        public string ImagePath { get; set; }
        public string Said { get; set; }
        /// <summary>
        /// 评价ID
        /// </summary>
        public int CommentId { get; set; }
    }

    

    public class Api_SureOrderModel
    {
        public string ProductId { get; set; }
        public int SaleNum { get; set; }
        public int Product_SkuId { get; set; }
        public Api_SureSkuDetailModel SkuDetail { get; set; }

    }

    public class Api_SureSkuDetailModel
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string ImgPath { get; set; }
        public int ShippingTemplateId { get; set; }
        public int Product_SkuId { get; set; }
        public decimal SallPrice { get; set; }
        public int StockNum { get; set; }
        public string ProppetyCombineName { get; set; }
        public int SkuType { get; set; }

        public float Weight { get; set; }

    }

  

}
