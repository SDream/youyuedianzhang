﻿using System;
using System.Collections.Generic;

namespace SuperiorModel
{
    class MarketingModel
    {
    }

    public class Api_CouponManagerModel
    {
        public Api_CouponManagerModel()
        {
            Coupons = new List<Api_CouponItem>();
            MyCoupons = new List<Api_UserCouponItem>();
        }
        public List<Api_CouponItem> Coupons { get; set; }
        public List<Api_UserCouponItem> MyCoupons { get; set; }
    }

    public class Api_CouponItem
    {
        public int Id { get; set; }
        public string CouponName { get; set; }
        public int CouponType { get; set; }
        public decimal DelAmount { get; set; }
        public int Discount { get; set; }
        public decimal MinOrderAmount { get; set; }
        public int Days { get; set; }
        public int TotalCount { get; set; }

    }
    public class Api_UserCouponItem
    {
        public int CouponId { get; set; }
        public string CouponName { get; set; }
        public int CouponType { get; set; }
        public decimal DelAmount { get; set; }
        public int Discount { get; set; }
        public decimal MinOrderAmount { get; set; }
        public DateTime CreateTime { get; set; }
        public int Days { get; set; }
        public DateTime ExpressTime
        {
            get
            {
                return this.CreateTime.AddDays(this.Days);
            }
        }
        public bool IsExpress
        {
            get
            {
                return DateTime.Now > this.ExpressTime;
            }
        }

    }
}
