﻿using System.Collections.Generic;

namespace SuperiorModel
{

    public class Api_HomeDataModel
    {
        public Api_HomeDataModel()
        {
            this.Banners = new List<Api_ProductItem>();
            this.NewRecommends = new List<Api_ProductItem>();
            this.Hots = new List<Api_ProductItem>();
        }
        /// <summary>
        /// 轮播商品集合
        /// </summary>
        public List<Api_ProductItem> Banners { get; set; }
        /// <summary>
        /// 最新推荐集合，
        /// </summary>
        public List<Api_ProductItem> NewRecommends { get; set; }
        /// <summary>
        /// 热门商品集合
        /// </summary>
        public List<Api_ProductItem> Hots { get; set; }
    }


    public class Api_ProductItem
    {
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string Title { get; set; }
        public string ImgPath { get; set; }
        public int SallNum { get; set; }
        public decimal SallPrice { get; set; }
        public string ShopAdminId { get; set; }

    }
}
