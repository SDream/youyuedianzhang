﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{

    public class ShopAppModel
    {
        /// <summary>
        /// 店铺ID
        /// </summary>
        public string Id { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string PaymentId { get; set; }
        public string PaySecret { get; set; }
        public string CreateTime { get; set; }
        public string UpdateTime { get; set; }
        /// <summary>
        /// 店主用户ID
        /// </summary>
        public string ShopAdminId { get; set; }
        public int AppType { get; set; }
        public string ServiceQQ { get; set; }
        public string ServiceWx { get; set; }
        [Required(ErrorMessage = "客服电话不能为空")]
        public string ServicePhone { get; set; }
        [Required(ErrorMessage = "店铺名称不能为空")]
        public string ShopName { get; set; }
        public string LoginName { get; set; }

        /// <summary>
        /// 店铺加密ID
        /// </summary>
        public string SecretId { get; set; }
        public int OptionStatus { get; set; }
        public string Remark { get; set; }
        /// <summary>
        /// 支付类型，1.商户自收，2.我司代收
        /// </summary>
        public int PayType { get; set; }
        /// <summary>
        /// 小程序商城过期时间
        /// </summary>

        public DateTime ProgrameExpireTime { get; set; }
        /// <summary>
        /// H5商城过期2时间
        /// </summary>
        public DateTime H5ExpireTime { get; set; }
        /// <summary>
        /// 小程序当前版本
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 小程序商城是否过期
        /// </summary>
        public bool ProgrameIsExpire
        {
            get
            {
                if (this.ProgrameExpireTime == null)
                {
                    return true;
                }
                else
                {
                    return DateTime.Now > this.ProgrameExpireTime;
                }
            }
        }
        /// <summary>
        /// H5商城是否过期
        /// </summary>
        public bool H5IsExpire
        {
            get
            {
                if (this.H5ExpireTime == null)
                {
                    return true;
                }
                else
                {
                    return DateTime.Now > this.H5ExpireTime;
                }
            }
        }

    }
}
