﻿using System;

namespace SuperiorModel
{
    class DataModel
    {
    }

    public class ShopOrderRankModel
    {
        public int ShopAdminId { get; set; }
        public string LoginName { get; set; }
        public int TotalOrderCount { get; set; }
        public int ProgramOrderCount { get; set; }
        public int H5OrderCount { get; set; }
    }
    public class ShopUserRankModel
    {
        public int ShopAdminId { get; set; }
        public string LoginName { get; set; }
        public int TotalUserCount { get; set; }
        public int ProgramUserCount { get; set; }
        public int H5UserCount { get; set; }
    }
    public class ShopAppExpressRankModel
    {
        public int Id { get; set; }
        public int ShopAdminId { get; set; }
        public string LoginName { get; set; }
        public int AppType { get; set; }
        public DateTime ExpressTime { get; set; }
        public string ShopName { get; set; }

    }
}
