﻿namespace SuperiorModel
{
    public class ProductCriteria:PagedBaseModel
    {
        public int ShopAdminId { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string MenuId { get; set; }
        public string LoginName { get; set; }
        public int OptionStatus { get; set; }

    }

    public class Api_ProductListCriteria
    {
        public string Spid { get; set; }
        public string keyWord { get; set; }
        public int OffSet { get; set; }
        public int Size { get; set; }
        public int MenuId { get; set; }
    }

    public class Api_ProductQrImageParms
    {
        public string ProductImage { get; set; }
        public string PECName { get; set; }
        public string PriceRanage { get; set; }
        public string ProductId { get; set; }
        public string ShopID { get; set; }
    }
}
