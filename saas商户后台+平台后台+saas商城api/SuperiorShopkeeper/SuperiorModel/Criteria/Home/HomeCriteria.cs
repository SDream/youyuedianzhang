﻿namespace SuperiorModel
{
    class HomeCriteria
    {
    }

    public class RechareOrderCriteria:PagedBaseModel
    {
        public string LoginName { get; set; }
        public string RechargeNo { get; set; }
        public int OrderStatus { get; set; }
        public int AppType { get; set; }
        public int SallType { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        public int ShopAdminId { get; set; }
        public string AccountManagerId { get; set; }

    }
}
