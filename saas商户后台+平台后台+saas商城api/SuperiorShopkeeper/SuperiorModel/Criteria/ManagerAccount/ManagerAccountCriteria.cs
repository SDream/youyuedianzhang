﻿namespace SuperiorModel
{
    public class ManagerAccountListCriteria:PagedBaseModel
    {
        public string PhoneNumber { get; set; }
        public string TrueName { get; set; }
        public int OptionStatus { get; set; }

    }
    public class ManagerAccountOrderListCriteria : PagedBaseModel
    {
        public string PhoneNumber { get; set; }
        public string TrueName { get; set; }
        public int OrderStatus { get; set; }
        public string OrderNo { get; set; }

    }
}
