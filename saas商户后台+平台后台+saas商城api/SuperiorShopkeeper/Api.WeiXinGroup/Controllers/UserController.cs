﻿using Newtonsoft.Json.Linq;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System.Web.Http;

namespace Api.WeiXinGroup.Controllers
{
    [ApiExceptionAttribute]
    public class UserController : ApiControllerBase
    {
        // GET: User
        private readonly IGroupUserService _IUserService;
        
        public UserController(IGroupUserService IUserService)
        {
            _IUserService = IUserService;
        }
        protected override void Dispose(bool disposing)
        {
            this._IUserService.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Login(Api_GroupUserLoginModel criteria)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            //通过code获取token和openid
            string authorJson = WeiXinHelper.GetOpenIdStr(ConfigSettings.Instance.APPID, ConfigSettings.Instance.APP_Secret, criteria.Code);
            LogManger.Instance.WriteLog("微信获取OPENID返回数据：" + authorJson);
            JObject jo = JObject.Parse(authorJson);
            string openid = jo["openid"].ToString();
            criteria.OpenId = openid;
            var res = _IUserService.Api_WxUserLoginOrRegister(criteria);
            if (string.IsNullOrEmpty(res.Token))
                return Error("用户请求异常");
            //存储token到redis,用于验证请求
            //RedisManager.Set<string>("Token_" + res.Id, res.Token, DateTime.Now.AddDays(24));
            return Success(res);
        }
        /// <summary>
        /// 用户收藏行为
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Collect(Api_CollectRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            var msg = "";
            var ret = _IUserService.Api_Collect(_userid.ToInt(),model.Type,model.BussinessId,out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 用户取消收藏行为
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> CancelCollect(int id)
        {
            _IUserService.CancelCollect(id);
            return Success(true);
        }
        /// <summary>
        /// 签到
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> Sign(string userId)
        {
            var _userid = base.ValidataParms(userId);
            if (_userid == null)
                return Error("非法参数");
            string msg = "";
            var ret = _IUserService.Api_Sign(_userid.ToInt(), out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
    }
}