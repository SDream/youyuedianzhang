﻿using SuperiorCommon;
using SuperiorModel;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Api.WeiXinGroup.Controllers
{
    public class ApiControllerBase : ApiController
    {
        /// <summary>
        /// 解密参数验证。
        /// </summary>
        /// <param name="secret"></param>
        /// <returns></returns>
        protected virtual string ValidataParms(string secret)
        {
            var res = 0;
            if (int.TryParse(SecretClass.DecryptQueryString(secret), out res))
            {
                return res.ToString();
            }
            return null;
        }

        /// <summary>
        /// 返回请求成功消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="res"></param>
        /// <returns></returns>
        protected virtual ApiResultModel<object> Success(object res)
        {
            return ApiResultModel<object>.Conclude(ApiResultEnum.Success, res);
        }
        /// <summary>
        /// 返回请求失败的消息
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected virtual ApiResultModel<object> Error(string msg)
        {
            var res = ApiResultModel<object>.Conclude(ApiResultEnum.Error, null, msg);
            return res;
        }

        protected virtual string ErrorMsg()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //获取所有错误的Key
            List<string> Keys = ModelState.Keys.ToList();
            //获取每一个key对应的ModelStateDictionary
            foreach (var key in Keys)
            {
                var errors = ModelState[key].Errors.ToList();
                //将错误描述添加到sb中
                foreach (var error in errors)
                {
                    sb.Append(error.ErrorMessage + "| ");
                }
            }
            return sb.ToString();
        }
        /// <summary>
        /// 返回给微信支付回调得信息。
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        protected virtual HttpResponseMessage RetMessage(object msg)
        {
            return new HttpResponseMessage
            {
                Content = new StringContent(msg.ToString(), new UTF8Encoding(false)
                  , "text/plain")
            };

        }


    }
}