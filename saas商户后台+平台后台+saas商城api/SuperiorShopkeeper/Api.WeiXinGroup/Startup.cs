﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Api.WeiXinGroup.Startup))]

namespace Api.WeiXinGroup
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
