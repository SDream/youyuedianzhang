﻿using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class ConsigneeDataAccess
    {
        public static int Add(Api_ConsigneeModel criteria)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@UserId", criteria.UserId.ToInt()),
                new SqlParameter("@ConsigneeName",criteria.ConsigneeName),
                new SqlParameter("@ConsigneePhone",criteria.ConsigneePhone),
                new SqlParameter("@ZipCode",criteria.ZipCode.IsNull()),
                new SqlParameter("@ProvinceCode ",criteria.ProvinceCode),
                new SqlParameter("@CityCode",criteria.CityCode),
                new SqlParameter("@AreaCode",criteria.AreaCode),
                new SqlParameter("@Address",criteria.Address),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "add_consignee", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static void Edit(Api_ConsigneeModel criteria)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@Id", criteria.Id),
                new SqlParameter("@ConsigneeName",criteria.ConsigneeName),
                new SqlParameter("@ConsigneePhone",criteria.ConsigneePhone),
                new SqlParameter("@ZipCode",criteria.ZipCode.IsNull()),
                new SqlParameter("@ProvinceCode ",criteria.ProvinceCode),
                new SqlParameter("@CityCode",criteria.CityCode),
                new SqlParameter("@AreaCode",criteria.AreaCode),
                new SqlParameter("@Address",criteria.Address),
                 new SqlParameter("@UserId",criteria.UserId.ToInt())
            };
            var sql = "update C_Consignee set ZipCode=@ZipCode,ProvinceCode=@ProvinceCode,CityCode=@CityCode,AreaCode=@AreaCode,ConsigneeName=@ConsigneeName,ConsigneePhone=@ConsigneePhone,Address=@Address where UserId = @UserId and Id = @Id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        public static void Del(int id, int userid)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id", id),
                 new SqlParameter("@userid",userid),
            };
            var sql = "update C_Consignee set IsDel=1 where Id=@id and UserId=@userid";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static void UpdateIsDefault(int id, int userid, int isdefault)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id", id),
                new SqlParameter("@userid",userid),
                new SqlParameter("@isdefault",isdefault)
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "update_consignee_isdefault", parms);
        }
        public static DataSet GetList(int userid)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                 new SqlParameter("@userid",userid),
            };
            var sql = "select Id,ConsigneeName,ConsigneePhone,IsDefault,Address,ZipCode,(select Area_Name from C_Areas where Area_Code =ProvinceCode ) as ProvinceName,(select Area_Name from C_Areas where Area_Code = CityCode) as CityName, (select Area_Name from C_Areas where Area_Code = AreaCode) as AreaName,ProvinceCode,CityCode,AreaCode from C_Consignee  where UserId=@userid and IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }
    }
}
