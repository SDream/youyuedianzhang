﻿using System.Collections.Generic;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data.SqlClient;
using System.Data;

namespace SuperiorShopDataAccess
{
    public class ShopCartDataAccess
    {
        /// <summary>
        /// 暂时不用
        /// </summary>
        /// <param name="critera"></param>
        /// <returns></returns>
        public static int Api_AddShopCart(Api_AddShopCartCriteria critera)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@userid",critera.UserId.ToInt()),
                new SqlParameter("@productId",critera.ProductId.ToInt()),
                new SqlParameter("@product_skuId",critera.Product_SkuId),
                new SqlParameter("@saleNum",critera.SaleNum),
                rtn_err
            };
            var ds = sqlManager.ExecuteDataset(CommandType.StoredProcedure, "add_shopcart", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static DataSet Api_GetShopCartList(string skuids)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {   
                new SqlParameter("@skuids",skuids)
            };
            var sql = "select ps.ProductId,ps.Id as Product_SkuId, p.ProductName,p.Brand,ps.StockNum,ps.SallPrice,ps.SkuType,ps.ProppetyCombineName,(select ImagePath from C_ProductImg where ProductId=ps.ProductId and IsDel=0 and IsDefault=1)as ImgPath,ps.IsDel,p.ShippingTemplateId,ps.Weight  from C_Product_Sku ps left join C_Product p on ps.ProductId = p.Id where ps.Id in(" + skuids+")";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static bool CreateOrderInsert(Api_CreateOrderCriteria criteria)
        {
            //下订单时，将商品临时插入表，这样存储过程就直接可以计算了
            var sqlManager = new SqlManager();
            var sqlList = new List<string>();
            var _userid = criteria.UserId.ToInt();
            sqlList.Add("update  C_ShoppingCart set IsDel=1 where UserId=" + _userid + ";");
            foreach (var item in criteria.Api_CreateOrder_ProductItems)
            {
                sqlList.Add(string.Format("insert into C_ShoppingCart(UserId,ProductId,Product_SkuId,SaleNum)values({0},{1},{2},{3});", _userid,item.ProductId.ToInt(),item.Product_SkuId,item.SaleNum));
            }
            return sqlManager.ExecTransactionAsUpdate_Public(sqlList.ToArray(), null);
        }

    }
}
