﻿using Snowflake.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorCommon
{
    public class IdPrivoder
    {
        private static IdWorker worker = new IdWorker(1, 1);

        public static long GetId()
        {
            var id = worker.NextId();
            return id;
        }
    }
}
