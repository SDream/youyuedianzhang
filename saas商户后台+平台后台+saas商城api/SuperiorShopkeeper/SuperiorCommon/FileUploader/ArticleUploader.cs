﻿namespace SuperiorCommon
{
    public class ArticleTitleImageUploader : MinDimensionUploader
    {
        public static readonly ArticleTitleImageUploader Instance = new ArticleTitleImageUploader();
        private ArticleTitleImageUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 2000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "ArticleTitleImage";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }

    public class ArticleDetailUploader : MinDimensionUploader
    {
        public static readonly ArticleDetailUploader Instance = new ArticleDetailUploader();
        private ArticleDetailUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 1000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "ArticleDetail";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }

    }

    public class GroupBannerUploader : MinDimensionUploader
    {
        public static readonly GroupBannerUploader Instance = new GroupBannerUploader();
        private GroupBannerUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 2000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "GroupBanner";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }
}
