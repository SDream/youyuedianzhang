﻿(function () {
    RoleListClass = {};
    var _optionType = 1;//操作类型，1添加，2修改
    var _id = "";//当前操作的ID
    RoleListClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addRole', this.Add);
            $(document).on('click', '.editRole', this.Edit);
            $(document).on('click', '.DeleteRole', this.Delete);
            $(document).on('click', '.setAuthority', this.SetAuthority);
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(btn_submit)', RoleListClass.Instance.Submit);
            });
            //-----end
        },
        SetAuthority: function () {
            var id = $(this).attr("OptionId");
            var iframeIndex = layer.open({
                type: 2,
                title: "权限控制",
                area: ['450px', '650px'],
                fixed: false, //不固定
                maxmin: true,
                btn: ['确认选择'],
                yes: function (index, layero) {
                    var iframeid = "layui-layer-iframe" + iframeIndex; //获取iframe分配的ID
                    var contens = $("#" + iframeid + "").contents();
                    var treeRes = document.getElementById(iframeid).contentWindow.GetDemoTree();
                    var _authorityJson = "";
                    //计算当前树的JSON字符串
                    if (treeRes.DemoTree.checkbarNode.length <= 0) {
                        //说明没有动，元权限JSON数据，存储
                        _authorityJson = JSON.stringify(treeRes.jsonList);
                    } else {
                        //有变动，将dtree选中的数组的数据给选中，不在选中里的设为不选中
                        for (var i = 0; i < treeRes.jsonList.length; i++) {
                            for (var j = 0; j < treeRes.DemoTree.checkbarNode.length; j++) {
                                if (treeRes.jsonList[i].id == parseInt(treeRes.DemoTree.checkbarNode[j].nodeId)) {
                                    treeRes.jsonList[i].checkArr[0].isChecked = "1";
                                    break;
                                } else {
                                    if (treeRes.jsonList[i].checkArr[0].isChecked == "1")
                                        treeRes.jsonList[i].checkArr[0].isChecked = "0";
                                }
                            }
                        }
                        _authorityJson = JSON.stringify(treeRes.jsonList);
                    }

                    var model = {};
                    model.Id = id;
                    model.Authoritys = _authorityJson;
                    RequestManager.Ajax.Post("/ShopAdmin/SetRoleAuthority", model, true, function (data) {
                        if (data.IsSuccess) {
                            layer.alert("执行成功", function () {
                                layer.closeAll();
                            })

                        } else {
                            layer.alert(data.Message);
                        }
                    })


                },
                content: '/ShopAdmin/CurrentRoleAuthority?id=' + id
            });
        },
        Add: function () {
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '240px'], //宽高
                content: RoleListClass.Instance.CreateHtml()

            });
            _optionType = 1;

        },

        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/ShopAdmin/DeleteRole?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/ShopAdmin/RoleList";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
        Edit: function () {
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '240px'], //宽高
                content: RoleListClass.Instance.CreateHtml()

            });
            _optionType = 2;
            _id = $(this).attr("OptionId");
            $("#text_rolename").val($(this).attr("RoleName"));

        },
        Submit: function () {
            var RoleName = $("#text_rolename").val();
            var model = {};
            var url = "";
            if (_optionType == 2) {
                model.Id = _id;
                url = "/ShopAdmin/EditRole";
            } else {
                url = "/ShopAdmin/AddRole";
            }

            model.RoleName = RoleName;
            var index = layer.load(1);
            RequestManager.Ajax.Post(url, model, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/ShopAdmin/RoleList";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        CreateHtml: function () {
            var arr = [];
            arr.push('<div class="layui-form" action="" >');
            arr.push('<div class="layui-form-item">');
            arr.push('<label for="text_rolename" class="layui-form-label">角色名称 </label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push('<input type="text" id="text_rolename" name="text_rolename" lay-verify="required" class="layui-input" placeholder="请输入角色名称">');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');
            arr.push('<div class="layui-form-item" style="text-align:center;">');
            arr.push('<div class="layui-input-block" style="margin:0;"><button class="layui-btn" lay-submit lay-filter="btn_submit">确定</button></div>');
            arr.push('</div>');
            arr.push('</div>');
            return arr.join('');
        }
    };
})();