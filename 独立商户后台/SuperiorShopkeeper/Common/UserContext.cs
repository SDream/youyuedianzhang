﻿using System.Web;
using SuperiorModel;
using SuperiorCommon;

namespace SuperiorShopkeeper
{
    /// <summary>
    /// 当前系统登录对象
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UserContext
    {
        /// <summary>
        /// 获取当前系统登录完整对象
        /// </summary>
        public static ShopInfoModel User
        {
            get
            {
                return HttpContext.Current.Session[SessionKey.ShopKey] as ShopInfoModel;
            }
        }
        /// <summary>
        /// 获取当前系统登录对象的解密ID
        /// </summary>
        public static string DeSecretId
        {
            get
            {
                return SecretClass.DecryptQueryString(User.Id);
            }
        }
    }
}