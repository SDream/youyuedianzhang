﻿using Redis;
using SuperiorCommon;
using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperiorShopkeeper
{
    public class ViewBagManager
    {
        public static bool IsCheck(string id, List<string> ids)
        {
            var res = false;
            foreach (var item in ids)
            {
                if (item == id)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }
        /// <summary>
        /// 判断当前选中的是否是全国
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="areas"></param>
        /// <returns></returns>
        public static bool IsAllArea(List<string> ids, List<AreaModel> areas)
        {
            var res = true;
            foreach (var item in areas)
            {
                if (!ids.Contains(item.Area_Code.ToString()))
                {
                    res = false;
                    break;
                }
            }
            return res;
        }
        /// <summary>
        /// 将省ID数组，转换成省字符串
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="areas"></param>
        /// <returns></returns>
        public static string GetAreaNameStr(List<string> ids, List<AreaModel> areas)
        {
            var res = "";
            if (IsAllArea(ids, areas))
            {
                res = "全国";
            }
            else
            {
                foreach (var id in ids)
                {
                    res += areas.Where(i=>i.Area_Code==id.ToInt()).FirstOrDefault().Area_Name+",";
                }
            }
            if (res.Contains(","))
                res.Remove(res.Length, 1);
            return res;
        }
       
    }
}