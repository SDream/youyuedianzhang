﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using Redis;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class SetController : BaseController
    {
        private readonly ISetService _ISetService;
        private readonly IAreaService _IAreaService;

        public SetController(ISetService ISetService, IAreaService IAreaService)
        {
            _ISetService = ISetService;
            _IAreaService = IAreaService;
        }
        protected override void Dispose(bool disposin)
        {
            this._ISetService.Dispose();
            this._IAreaService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Set
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 运费模板列表
        /// </summary>
        /// <returns></returns>
        /// 
       [ShopAuthority(AuthorityMenuEnum.ShippingTemplateList)]
        public ActionResult ShippingTemplateList()
        {
            var model = _ISetService.GetShippingTemplateList(UserContext.DeSecretId.ToInt());
            return View(model);
        }
        /// <summary>
        /// 添加运费模板
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return View();
        }
        /// <summary>
        /// 选择区域子页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CurrentAreas(string ids)
        {
            var areas = _IAreaService.Get(0);
            //只显示当前模板的可选区域
            var _tempList = ids.Split(',').ToList();
            areas = areas.Where(p => !_tempList.Contains(p.Area_Code.ToString())).ToList();
            return View(areas);
        }
        /// <summary>
        /// 添加运费模板
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.AddTemplate)]
        public JsonResult AddTemplate(ShippingTemplateModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            var res = _ISetService.AddShippingTemplate(model);
            if (!res)
                return Error("添加失败");
            return Success(true);
        }
        /// <summary>
        /// 删除模板
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.DelTemplate)]
        public JsonResult DelTemplate(int id)
        {
            string msg = "";
            var ret = _ISetService.DelShippingTemplate(id, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 编辑运费模板页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditSt(int id)
        {
            var model = _ISetService.GetShippingTemplateModel(id);
            var provinceIds = new List<string>();
            model.Items.ForEach((item) =>
            {
                provinceIds.AddRange(item.ProvinceIdList);
            });
            ViewBag.Pids = string.Join(",", provinceIds);
            return View(model);
        }
        /// <summary>
        /// 提交编辑运费模板
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.EditTemplate)]
        public JsonResult EditTemplate(ShippingTemplateModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            var res = _ISetService.EditShippingTemplate(model);
            if (!res)
                return Error("修改失败");
            //控制redis
            RedisManager.Remove("ShippingTemplateModel_" + model.Id);
            return Success(true);
        }
    }
}