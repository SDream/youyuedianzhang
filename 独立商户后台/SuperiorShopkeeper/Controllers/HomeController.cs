﻿using SuperiorModel;
using SuperiorShopBussinessService;
using System.Web.Mvc;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class HomeController : BaseController
    {
        private readonly IHomeService _IHomeService;
        private readonly IShopService _IShopService;

        public HomeController(IHomeService IHomeService, IShopService IShopService)
        {
            _IHomeService = IHomeService;
            _IShopService = IShopService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IHomeService.Dispose();
            this._IShopService.Dispose();   
            base.Dispose(disposin);
        }

        // GET: Home
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult _Index()
        {
            var model = _IHomeService.GetHomeData(UserContext.DeSecretId.ToInt());
            return View(model);
        }
        /// <summary>
        /// 获取近7日每日用户增长量报表数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetAddUserChart()
        {
            var model = _IHomeService.GetAddUserChart(UserContext.DeSecretId.ToInt());
            return Success(model);
        }

        /// <summary>
        /// 获取近7日每日销售额和销售量报表数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetSallChart()
        {
            var model = _IHomeService.GetSallChart(UserContext.DeSecretId.ToInt());
            return Success(model);
        }
        
        /// <summary>
        /// 操作文档
        /// </summary>
        /// <returns></returns>
        public ActionResult OptionWord()
        {
            return View();
        }
        /// <summary>
        /// 常见问题
        /// </summary>
        /// <returns></returns>
        public ActionResult Questions()
        {
            return View();
        }
    }
}