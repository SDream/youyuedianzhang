﻿namespace SuperiorModel
{
    public enum ApiResultEnum
    {
        Error = 0,
        Success = 200
        
    }
    public class ApiResultModel<T>
    {
        public ApiResultModel(ApiResultEnum status, string message, T result)
        {
            this.Code = (int)status;
            this.Message = message;
            this.Data = result;
        }

        public static ApiResultModel<T> Conclude(ApiResultEnum status)
        {
            return Conclude(status, default(T));
        }

        public static ApiResultModel<T> Conclude(ApiResultEnum status, T result)
        {
            return new ApiResultModel<T>(status, "", result);
        }

        public static ApiResultModel<T> Conclude(ApiResultEnum status, T result, string message)
        {
            return new ApiResultModel<T>(status, message, result);
        }

        public int Code { get;  set; }
        public string Message { get;  set; }
        public T Data { get;  set; }
    }
}
