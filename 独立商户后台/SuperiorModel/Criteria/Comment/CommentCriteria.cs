﻿namespace SuperiorModel
{
    public class SearchCommentCriteria:PagedBaseModel
    {
        public int CommentLevel { get; set; }
        public string OrderNo { get; set; }
        public string LoginName { get; set; }
        public int ShopAdminId { get; set; }
    }
}
