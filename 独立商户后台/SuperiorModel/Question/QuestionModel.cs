﻿namespace SuperiorModel
{
    public class QuestionModel
    {
        public int Id { get; set; }
        public int ShopAppId { get; set; }
        public int ShopAdminId { get; set; }
        public string LoginName { get; set; }
        public int UserId { get; set; }
        public string NickName { get; set; }
        public string ShopName { get; set; }
        public string Question { get; set; }
        public string CreateTime { get; set; }
    }
    public class QuestionParms
    {
        public int ShopAppId { get; set; }
        public int ShopAdminId { get; set; }
        public int UserId { get; set; }
        public string Question { get; set; }
    }
}
