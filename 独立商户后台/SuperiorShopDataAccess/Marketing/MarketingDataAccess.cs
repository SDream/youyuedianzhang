﻿using System.Collections.Generic;
using System.Text;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;
using SuperiorModel.Criteria.Marketing;

namespace SuperiorShopDataAccess
{
    public class MarketingDataAccess
    {
        public static DataSet GetSetModel(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_FullAmountSet where ShopAdminId=@shopAdminId;";
            sql += " DECLARE @pidlist varchar(500) ";
            sql += " declare @areaList varchar(500) ";
            sql += " select @pidlist= NotHasProductIds,@areaList= NotHasProvinceIds from C_FullAmountSet where ShopAdminId=@shopAdminId ";
            sql += " EXEC('select ImagePath from C_ProductImg where ProductId in('+@pidlist+') and IsDefault=1 and IsDel=0') ";
            sql += " EXEC('select Area_Name from C_Areas where Area_Code in('+@areaList+')') ";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void SetNotPids(string ids, int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@ids",ids)
            };
            var sb = new StringBuilder();
            sb.Append("if exists(select Id from C_FullAmountSet where ShopAdminId=@shopAdminId)");
            sb.Append(" begin ");
            sb.Append(" update C_FullAmountSet set NotHasProductIds=@ids where ShopAdminId=@shopAdminId ");
            sb.Append(" end ");
            sb.Append(" else ");
            sb.Append(" begin ");
            sb.Append(" insert into C_FullAmountSet(ShopAdminId,NotHasProductIds)values(@shopAdminId,@ids) ");
            sb.Append(" end ");
            sqlManager.ExecuteNonQuery(CommandType.Text, sb.ToString(), parms);
        }
        public static void SetNotAreas(string ids, int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@ids",ids)
            };
            var sb = new StringBuilder();
            sb.Append("if exists(select Id from C_FullAmountSet where ShopAdminId=@shopAdminId)");
            sb.Append(" begin ");
            sb.Append(" update C_FullAmountSet set NotHasProvinceIds=@ids where ShopAdminId=@shopAdminId ");
            sb.Append(" end ");
            sb.Append(" else ");
            sb.Append(" begin ");
            sb.Append(" insert into C_FullAmountSet(ShopAdminId,NotHasProvinceIds)values(@shopAdminId,@ids) ");
            sb.Append(" end ");
            sqlManager.ExecuteNonQuery(CommandType.Text, sb.ToString(), parms);
        }
        public static void SetFullAmount(int optionStatus, int amount, int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@optionStatus",optionStatus),
                new SqlParameter("@amount",amount),
                  new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sb = new StringBuilder();
            sb.Append("if exists(select Id from C_FullAmountSet where ShopAdminId=@shopAdminId)");
            sb.Append(" begin ");
            sb.Append(" update C_FullAmountSet set OptionStatus=@optionStatus,Amount=@amount where ShopAdminId=@shopAdminId ");
            sb.Append(" end ");
            sb.Append(" else ");
            sb.Append(" begin ");
            sb.Append(" insert into C_FullAmountSet(ShopAdminId,OptionStatus,Amount)values(@shopAdminId,@optionStatus,@amount) ");
            sb.Append(" end ");
            sqlManager.ExecuteNonQuery(CommandType.Text, sb.ToString(), parms);
        }

        public static DataSet GetCouponList(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select *,isnull((select COUNT(*) from C_UserCoupon where CouponId=c.Id),0)as GetCount from C_Coupon c where ShopAdminId=@shopAdminId and IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        

        public static void AddCoupon(CouponModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@ShopAdminId",model.ShopAdminId),
                new SqlParameter("@CouponName",model.CouponName),
                 new SqlParameter("@CouponType",model.CouponType),
                  new SqlParameter("@DelAmount",model.DelAmount),
                   new SqlParameter("@Discount",model.Discount),
                    new SqlParameter("@MinOrderAmount",model.MinOrderAmount),
                     new SqlParameter("@Days",model.Days),
                      new SqlParameter("@TotalCount",model.TotalCount),
            };
            var sql = "insert into C_Coupon(ShopAdminId,CouponName,CouponType,DelAmount,Discount,MinOrderAmount,Days,TotalCount)values(@ShopAdminId,@CouponName,@CouponType,@DelAmount,@Discount,@MinOrderAmount,@Days,@TotalCount)";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static void DelCoupon(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "update C_Coupon set IsDel=1 where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static DataSet SearchUserCouponList(UserCouponCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            sb.Append(" and cc.ShopAdminId=@ShopAdminId ");
            parms.Add(new SqlParameter("@ShopAdminId", criteria.ShopAdminId));
            if (!string.IsNullOrEmpty(criteria.NickName))
            {
                sb.Append(" and cu.NickName=@nickname ");
                parms.Add(new SqlParameter("@nickname", criteria.NickName));
            }
            if (criteria.CouponType != 999)
            {
                sb.Append(" and cc.CouponType=@coupontype ");
                parms.Add(new SqlParameter("@coupontype", criteria.CouponType));
            }
          
            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over (order by cuc.Id desc) as rownum ,cu.NickName,cuc.CouponId,cc.CouponName, cc.CouponType,cuc.CreateTime  from C_UserCoupon cuc left join C_Users cu on cuc.UserId=cu.Id left join C_Coupon cc on cuc.CouponId=cc.Id where {1})tt where tt.rownum>{2}; select count(*) as totalCount from C_UserCoupon cuc left join C_Users cu on cuc.UserId=cu.Id left join C_Coupon cc on cuc.CouponId=cc.Id where {1} ", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

       
    }
}
