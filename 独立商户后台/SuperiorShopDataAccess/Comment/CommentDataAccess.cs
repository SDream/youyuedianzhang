﻿using System.Collections.Generic;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;


namespace SuperiorShopDataAccess
{
    public class CommentDataAccess
    {

        public static DataSet Search(SearchCommentCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.OrderNo))
            {
                sb.Append(" and o.OrderNo=@OrderNo");
                parms.Add(new SqlParameter("@OrderNo", criteria.OrderNo));
            }
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and c.LoginName=@LoginName ");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }

            if (criteria.CommentLevel != 0)
            {
                sb.Append(" and c.CommentLevel=@CommentLevel ");
                parms.Add(new SqlParameter("@CommentLevel", criteria.CommentLevel));
            }
            if (criteria.ShopAdminId != criteria.ShopAdminId)
            {
                sb.Append(" and c.ShopAdminId=@ShopAdminId ");
                parms.Add(new SqlParameter("@ShopAdminId", criteria.ShopAdminId));
            }

            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by c.Id desc)as    rownum,c.Id,u.NickName,u.HeadImgUrl,o.OrderNo,c.LoginName,s.ShopName,c.CommentLevel, c.CommentMsg,c.CreateTime,p.ProductName from C_Comment c left join C_Order o on c.OrderId=o.Id left join C_Users u on c.UserId= u.Id left join C_ShopApp s on c.ShopAppId=s.Id left join C_Product p on c.ProductId = p.Id where {1} and c.IsDel=0 )tt where tt.rownum>{2};select count(*) as totalCount from  C_Comment c left join C_Order o on c.OrderId=o.Id left join C_Users u on c.UserId= u.Id left join C_ShopApp s on c.ShopAppId=s.Id left join C_Product p on c.ProductId = p.Id  where {1} and c.IsDel=0", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static void Del(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
           {
                new SqlParameter("@id",id)
            };
            var sql = "update C_Comment set IsDel=1 where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        
    }
}
