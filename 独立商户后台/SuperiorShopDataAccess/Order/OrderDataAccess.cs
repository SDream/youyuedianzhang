﻿using SuperiorModel;
using SuperiorSqlTools;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class OrderDataAccess
    {
        public static DataSet Search(OrderCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();

            if (criteria.ShopAppId != "unChecke")
            {
                sb.Append(" and o.ShopAppId=@ShopAppId ");
                parms.Add(new SqlParameter("@ShopAppId", criteria.ShopAppId.ToInt()));
            }
            if (criteria.ShopAdminId != 0)
            {
                sb.Append(" and o.ShopAdminId=@ShopAdminId ");
                parms.Add(new SqlParameter("@ShopAdminId", criteria.ShopAdminId));
            }
            if (!string.IsNullOrEmpty(criteria.OrderNo))
            {
                sb.Append(" and o.OrderNo=@OrderNo ");
                parms.Add(new SqlParameter("@OrderNo", criteria.OrderNo));
            }
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and o.LoginName=@LoginName ");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }
            if (criteria.OrderStatus != 999)
            {
                sb.Append(" and o.OrderStatus=@OrderStatus ");
                parms.Add(new SqlParameter("@OrderStatus", criteria.OrderStatus));

            }
            if (!string.IsNullOrEmpty(criteria.BeginTime))
            {
                sb.Append(" and o.CreateTime>=@BeginTime ");
                parms.Add(new SqlParameter("@BeginTime", criteria.BeginTime));
            }
            if (!string.IsNullOrEmpty(criteria.EndTime))
            {
                sb.Append(" and o.CreateTime<=@EndTime ");
                parms.Add(new SqlParameter("@EndTime", criteria.EndTime));
            }
            var sql = string.Format("select top({0}) * from (select ROW_NUMBER() over(order by o.Id desc)as rownum,ISNULL(s.ShopName,'') as ShopName,u.NickName,o.Id,o.OrderNo,o.RealityAmount,o.PayOderNo,o.OrderStatus,o.CreateTime,o.PayTime,o.SendTime,o.FailReason,o.ServiceStatus,o.LoginName from C_Order o left join C_ShopApp s on o.ShopAppId=s.Id left join C_Users u on o.UserId=u.Id where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_Order o where {1} ", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static DataSet GetDetail(int orderid)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderid",orderid)
            };
            var sql = "select * from C_Order where Id=@orderid;select ProductName, BuyNum, SalePrice, ProppetyCombineName, SkuCode from C_OrderDetails where OrderId = @orderid";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void SendProduct(SendProductParms model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderid",model.Id.ToInt()),
                new SqlParameter("@DeliveryName",model.DeliveryName),
                new SqlParameter("@DeliveryNo",model.DeliveryNo),
            };
            var sql = "update C_Order set OrderStatus = 1,SendTime=getdate(),SendStatus=1,DeliveryName=@DeliveryName,DeliveryNo=@DeliveryNo where Id=@orderid";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static void UpdateDelivery(SendProductParms model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderid",model.Id.ToInt()),
                new SqlParameter("@DeliveryName",model.DeliveryName),
                new SqlParameter("@DeliveryNo",model.DeliveryNo),
            };
            var sql = "update C_Order set DeliveryName=@DeliveryName,DeliveryNo=@DeliveryNo where Id=@orderid";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static void FinishService(int orderid)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderid",orderid)
            };
            var sql = "update C_Order set ServiceStatus = 1,ServiceTime=getdate() where Id=@orderid";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

       
    }
}
