﻿namespace SuperiorCommon
{
    /// <summary>
    /// JSON返回结果（用于菜单接口等）
    /// </summary>
    public class WxJsonResult
    {
        public ReturnCode errcode { get; set; }
        public string errmsg { get; set; }

    }

    public class Wx_SubimitResultModel
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        /// <summary>
        /// 提交返回得审核编号
        /// </summary>
        public int auditid { get; set; }
    }

    public class Wx_CheckResult
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        /// <summary>
        /// 其中0为审核成功，1为审核失败，2为审核中，3已撤回
        /// </summary>
        public int status { get; set; }
        public string reason { get; set; }
        public string screenshot { get; set; }

    }
}
