﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface ICommentService:IService
    {
        PagedSqlList<CommentModel> Search(SearchCommentCriteria criteria);
        void Del(int id);
    }
}
