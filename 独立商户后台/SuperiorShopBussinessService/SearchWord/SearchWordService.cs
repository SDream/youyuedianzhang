﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperiorModel;
using SuperiorShopDataAccess;

namespace SuperiorShopBussinessService
{
    public class SearchWordService : ISearchWordService
    {
        public void Add(SearchWordModel model)
        {
            SearchWordDataAccess.Add(model);
        }

        public void Del(int id)
        {
            SearchWordDataAccess.Del(id);
        }

        public void Dispose()
        {
            
        }

        public List<SearchWordModel> GetList(int shopAdminId)
        {
            var res = new List<SearchWordModel>();
            var ds = SearchWordDataAccess.GetListDs(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new SearchWordModel()
                        {
                            KeyWord = dr["KeyWord"].ToString(),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            Id = Convert.ToInt32(dr["Id"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"])
                        });
                    }
                }
            }
            return res;
        }

        public void Option(int id, int option)
        {
            SearchWordDataAccess.Option(id,option);
        }
    }
}
