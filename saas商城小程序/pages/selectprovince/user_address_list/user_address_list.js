
const app = getApp();
import TestData from '../../../mock/testData.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    _type: null, //type= 1 订单确认页面进入
    MobileConsigneeGetList: null,
    // 触摸开始时间
    touchStartTime: 0,
    // 触摸结束时间
    touchEndTime: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu()
    this.data._type = options.type;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },
  getList:function(){
    var that = this;
    wx.showLoading({
      title: '读取地址列表中 ',
    })
    wx.request({

      url: app.appSetting.host + "/api/ConSignee/GetList?userid=" + app.globalData.userInfo.Id,
      data: {
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            MobileConsigneeGetList: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  goToAdd: function () {
    wx.navigateTo({
      url: "/pages/selectprovince/user_address_add/user_address_add",
    })
  },
  bingLongTap(ev) {
    let self = this;
    wx.showActionSheet({
      itemList: ['编辑', '删除', '默认'],
      itemColor: '#333',
      success: function (e) {
        if (e.tapIndex == 1) {
          TestData.MobileConsigneeDelete(ev.currentTarget.dataset.addressinfo.Id).then((res) => {
            if (ev.currentTarget.dataset.addressinfo.Id == app.globalData.defaultAddress.Id) {
              app.globalData.defaultAddress = null;
            }
            wx.showToast({
              title: '删除成功',
              icon: 'none'
            })
            self.getList();
          });
        } else if (e.tapIndex == 2) {

          TestData.MobileConsigneeUpdateState({
            Id: ev.currentTarget.dataset.addressinfo.Id,
            State: (ev.currentTarget.dataset.addressinfo.IsDefault==1?0:1)
          }).then((res) => {
            wx.showToast({
              title: '更新成功',
              icon: 'none'
            })
            self.getList();
          });

        } else if (e.tapIndex == 0) {
          wx.navigateTo({
            url: "/pages/selectprovince/user_address_add/user_address_add?type=1&addressInfo=" + JSON.stringify(ev.currentTarget.dataset.addressinfo),
          })
        } else {

        }
      }
    })
  },
  /// 单击、双击
  multipleTap: function (ev) {
    var that = this
    if (that.touchEndTime - that.touchStartTime < 350) {
      if (that.data._type == 1) {
        app.globalData.defaultAddress = ev.currentTarget.dataset.addressinfo
        that.setData({
          app: {
            globalData: {
              defaultAddress: ev.currentTarget.dataset.addressinfo
            }
          }
        });
        wx.navigateBack({

        })
      }
    }
  },
  /// 按钮触摸开始触发的事件
  touchStart: function (e) {
    this.touchStartTime = e.timeStamp
  },

  /// 按钮触摸结束触发的事件
  touchEnd: function (e) {
    this.touchEndTime = e.timeStamp
  }
})