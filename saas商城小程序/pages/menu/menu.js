// pages/menu/menu.js
const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    getListParam: {
      keyWord: '',
      OffSet: 0,
      Size: 6,
      menuId:0,
      list: []
    },

    lastRequestCount: 6
  },
  btnSearch: function () {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount: 6
    });
    this.initgoodsList();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.hideShareMenu();
    this.setData({
      'getListParam.menuId': options.menuid
    });
    this.initgoodsList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount:6
    });
    this.initgoodsList('stopPullDownRefresh');
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.setData({
      'getListParam.OffSet': this.data.getListParam.OffSet + 1
    });
    this.initgoodsList('onReachBottom');
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  initgoodsList: function(type) {
    if (this.data.lastRequestCount < this.data.getListParam.Size)
      return false;//防止到最后一页继续请求
    var that = this;
    wx.showLoading({
      title: '读取商品中',
    })
    wx.request({
      url: app.appSetting.host + "/api/product/GetProductList",
      data: {
        Spid: app.appSetting.spid,
        keyWord: that.data.getListParam.keyWord,
        OffSet: that.data.getListParam.OffSet,
        Size: that.data.getListParam.Size,
        MenuId: that.data.getListParam.menuId
      },
      method: 'POST',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            lastRequestCount: res.data.Data.length
          });
          if (type == 'stopPullDownRefresh') {
            that.setData({
              'getListParam.list': res.data.Data
            });
            wx.stopPullDownRefresh()
          }
          if (type == 'onReachBottom') {
            that.setData({
              'getListParam.list': that.data.getListParam.list.concat(res.data.Data)
            });
          } else {
            that.setData({
              'getListParam.list': res.data.Data
            });
          }
        }else{
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })

  },
  keyWordChange: function(ev) {
    this.setData({
      'getListParam.keyWord': ev.detail.value
    });
  }
})